﻿using System;
using System.Collections.Generic;
using System.Linq;

using FluentAssertions;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace TinyStateMachine.UnitTests.StateConfigurations
{
    [TestFixture]
    public class StateMachineConfiguration_when_setting_state_name
    {
        private StateMachineConfiguration<string> machineConfig;

        [SetUp]
        public void Init()
        {
            machineConfig = TestHelper.CreateStateMachineConfiguration<string>();
        }

        [Test]
        public void Name_should_be_stored()
        {
            var config = machineConfig.ConfigureState("hello").WithName("mystate");
            config.Configuration.Name.Should().Be("mystate");
        }

        [Test]
        public void Name_should_be_derived_from_state_value_when_not_specified()
        {
            var config = machineConfig.ConfigureState("hello");
            config.Configuration.Name.Should().Be("hello");
        }
    }
}



