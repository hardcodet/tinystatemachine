using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateConfigurations
{
    [TestFixture]
    public class StateMachineConfiguration_when_configuring_state_in_multiple_steps
    {
        [Test]
        public void Existing_configuration_object_should_be_reused()
        {
            var machineConfig = TestHelper.CreateStateMachineConfiguration<string>();
            var stateConfig1 = machineConfig.ConfigureState("foo");
            var stateConfig2 = machineConfig.ConfigureState("bar");
            var stateConfig3 = machineConfig.ConfigureState("foo");

            stateConfig1.Configuration.Should().NotBeSameAs(stateConfig2.Configuration);
            stateConfig1.Configuration.Should().BeSameAs(stateConfig3.Configuration);
        }
    }
}