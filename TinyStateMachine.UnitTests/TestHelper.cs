using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TinyStateMachine.UnitTests
{
    public static class TestHelper
    {
        public static StateMachineConfiguration<T> CreateStateMachineConfiguration<T>(bool strict = true, IEqualityComparer<T> comparer = null)
        {
            return new StateMachineConfiguration<T>(strict, comparer);
        }

        public static StateMachine<T> CreateStateMachine<T>(T initialState, string initialStateName = "initial", bool strict = true)
        {
            var configuration = CreateStateMachineConfiguration<T>(strict);
            configuration.ConfigureState(initialState).WithName(initialStateName);
            return new StateMachine<T>(configuration, initialState);
        }
        
        internal static State<T> CreateState<T>(T stateValue)
        {
            var configuration = new StateConfiguration<T>(stateValue, "test", EqualityComparer<T>.Default);
            return new State<T>(stateValue, configuration);
        }


        /// <summary>
        /// Performs a <see cref="Task.Wait()"/> and swallows an
        /// <see cref="AggregateException"/> if it occurs, allowing
        /// the waiting code to just inspect the task's properties
        /// after completion.
        /// </summary>
        /// <param name="task">The task to wait upon.</param>
        /// <returns>True if the tasked completed properly. False in case
        /// of an exception or cancelation.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="task"/>
        /// is a null reference.</exception>
        public static bool WaitWithoutThrow(this Task task)
        {
            try
            {
                task.Wait();
                return true;
            }
            catch (AggregateException)
            {
                return false;
            }
        }
    }
}