﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using FluentAssertions;
using NUnit.Framework;
using TinyStateMachine.Util;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_switching_to_different_state : LooseStateMachineTestBase
    {
        [Test]
        public void New_state_should_be_assigned_to_statemachine()
        {
            IStateTransition<int> si = StateMachine.GoToStateAsync(23).Result;

            si.NewState.Value.Should().Be(23);
            StateMachine.CurrentState.Should().BeSameAs(si.NewState);
        }

        [Test]
        public void Transition_object_should_provide_new_and_previous_states()
        {
            StateMachine.GoToStateAsync(1).Wait();
            IState<int> currentState = StateMachine.CurrentState;

            IStateTransition<int> transition = StateMachine.GoToStateAsync(2).Result;
            transition.PreviousState.Should().BeSameAs(currentState);
            transition.NewState.Should().BeSameAs(StateMachine.CurrentState);
            transition.NewState.Value.Should().Be(2);
        }

        [Test]
        public void Entry_action_should_be_fired()
        {
            bool called = false;
            StateMachine.Configuration.ConfigureState(333).OnEntry(ti => called = true);

            StateMachine.GoToStateAsync(333).Wait();
            called.Should().BeTrue();
        }

        [Test]
        public void Exit_action_should_be_fired()
        {
            bool called = false;
            StateMachine.Configuration.ConfigureState(333).OnExit(ti => called = true);

            StateMachine.GoToStateAsync(333).Wait();
            called.Should().BeFalse();

            StateMachine.GoToStateAsync(444).Wait();
            called.Should().BeTrue();
        }

        [Test]
        public void Parameter_should_indicate_that_not_specified()
        {
            IStateTransition<int> transition = StateMachine.GoToStateAsync(789).Result;
            transition.NewState.Parameter.HasValue.Should().BeFalse();
        }


        [Test]
        public void Accessing_unspecified_parameter_value_should_throw_exception()
        {
            IStateTransition<int> transition = StateMachine.GoToStateAsync(789).Result;
            transition.NewState.Parameter.HasValue.Should().BeFalse();
            Action a = () =>
            {
                var x = transition.NewState.Parameter.Value;
            };

            a.ShouldThrow<InvalidOperationException>();
        }

        [Test]
        public void Parameter_should_be_stored_if_specified()
        {
            IStateTransition<int> transition = StateMachine.GoToStateAsync(789, "hello world").Result;
            transition.NewState.Parameter.HasValue.Should().BeTrue();
            transition.NewState.Parameter.Value.Should().Be("hello world");
        }
    }
}



