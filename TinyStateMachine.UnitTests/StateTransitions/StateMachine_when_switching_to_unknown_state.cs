﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_switching_to_unknown_state
    {
        [SetUp]
        public void Init()
        {
            
        }
        
        [Test]
        public void Strict_machine_should_throw_exception_in_strict_mode()
        {
            var stateMachine = TestHelper.CreateStateMachine("hello", "start", true);

            Action action = () => stateMachine.GoToStateAsync("world").Wait();
            action.ShouldThrow<InvalidOperationException>();
        }


        [Test]
        public void State_machine_should_keep_previous_state_if_transition_not_allowed()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }

        [Test]
        public void Loose_machine_should_create_adhoc_configuration()
        {
            var stateMachine = TestHelper.CreateStateMachine("hello", "start", false);

            stateMachine.GoToStateAsync("world").Wait();
            stateMachine.CurrentState.Should().NotBeNull();
            stateMachine.CurrentState.Value.Should().Be("world");
        }

        [Test]
        public void Loose_machine_without_any_states_should_work()
        {
            var configuration = TestHelper.CreateStateMachineConfiguration<string>(false);
            var stateMachine = new StateMachine<string>(configuration, "hello");

            stateMachine.CurrentState.Should().NotBeNull();
            stateMachine.CurrentState.Value.Should().Be("hello");

            stateMachine.GoToStateAsync("world").Wait();
            stateMachine.CurrentState.Should().NotBeNull();
            stateMachine.CurrentState.Value.Should().Be("world");
        }

    }
}