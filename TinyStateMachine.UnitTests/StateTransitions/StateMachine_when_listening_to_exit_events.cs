using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_listening_to_exit_events : LooseStateMachineTestBase
    {
        private List<int> states;

        [SetUp]
        public void Init()
        {
            states = new List<int>();
        }

        [Test]
        public void Exit_listener_should_not_be_invoked_when_exiting_other_states()
        {
            StateMachine.Configuration.ConfigureState(1)
                .OnExit(ti => states.Add(ti.PreviousState.Value));

            StateMachine.GoToStateAsync(2).Wait();
            StateMachine.GoToStateAsync(3).Wait();

            states.Should().BeEmpty();
        }

        [Test]
        public void Exit_listener_should_not_be_invoked_when_entering_state()
        {
            StateMachine.Configuration.ConfigureState(1)
                .OnExit(ti => states.Add(ti.PreviousState.Value));

            StateMachine.GoToStateAsync(2).Wait();
            StateMachine.GoToStateAsync(1).Wait(); //entering!

            states.Should().BeEmpty();
        }

        [Test]
        public void Exit_listener_should_be_invoked_when_transitioning_out()
        {
            StateMachine.Configuration.ConfigureState(1)
                .OnExit(ti => states.Add(ti.PreviousState.Value));

            StateMachine.GoToStateAsync(1).Wait();
            states.Should().BeEmpty();

            StateMachine.GoToStateAsync(2).Wait();
            states.Single().Should().Be(1);
        }

        [Test]
        public void Filtering_from_specified_source_state_should_only_trigger_if_source_matches()
        {
            //exit from 2 -> 1
            StateMachine.Configuration.ConfigureState(2)
                .OnExitTo(1, ti => states.Add(ti.PreviousState.Value));

            //enter state
            StateMachine.GoToStateAsync(2).Wait();
            states.Should().BeEmpty();

            //go to different state
            StateMachine.GoToStateAsync(3).Wait();
            states.Should().BeEmpty();

            //go to 1 from 3
            StateMachine.GoToStateAsync(1).Wait();
            states.Should().BeEmpty();

            //enter from 1 (no listener)
            StateMachine.GoToStateAsync(2).Wait();
            states.Should().BeEmpty();

            //go to 1
            StateMachine.GoToStateAsync(1).Wait();
            states.Single().Should().Be(2);
        }
    }
}