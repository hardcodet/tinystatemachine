﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_firing_transition_event : LooseStateMachineTestBase
    {
        [Test]
        public void CurrentState_should_have_been_updated()
        {
            bool fired = false;
            StateMachine.StateTransition += ti =>
            {
                fired = true;
                ti.StateMachine.Should().BeSameAs(StateMachine);
                ti.StateMachine.CurrentState.Should().BeSameAs(ti.NewState);
            };

            //StateMachine.Configuration.ConfigureState(123);

            StateMachine.GoToStateAsync(123).Wait();
            fired.Should().BeTrue("Event not fired.");
        }

        [Test]
        public void Name3()
        {
            var sm = new StateMachine<string>("hello");

            bool invoked = false;

            sm.OnTransition("hello", "world", ti => invoked = true);
            
            //sm.OnTransitionTo(...)
            //sm.OnTransitionFrom(...)       

            sm.GoToStateAsync("world").Wait();
            invoked.Should().BeTrue();
            sm.CurrentState.Value.Should().Be("world");
        }

        [Test]
        public void Name()
        {
            var sm = new StateMachine<string>("hello");

            bool invoked = false;

            sm.StateTransition += ti =>
            {
                invoked = true;
                ti.NewState.Value.Should().Be("world");
            };
            
            //sm.OnTransitionTo(...)
            //sm.OnTransitionFrom(...)       
            
            sm.GoToStateAsync("world").Wait();
            invoked.Should().BeTrue();
            sm.CurrentState.Value.Should().Be("world");
        }

        [Test]
        public void Delegates_Test()
        {
            List<Action<string>> list = new List<Action<string>>();
            list.Add(SayHello);
            bool success = list.Remove(SayHello);

            success.Should().BeTrue();
            list.Should().BeEmpty();

            list.Add(SayHello);
            list.Add(SayHello);
            list.First().Should().Be(list.Last());
        }

        private void SayHello(string obj)
        {
            
        }

        [Test]
        public async void Async_Test()
        {
            Task<string> task = Foo();
            try
            {
                string result = await task;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            
        }

        private async Task<string> Foo()
        {
            var task = Task.Run<string>(() =>
            {
                throw new NotSupportedException();
                return "";
            });

            try
            {
                TaskAwaiter<string> awaiter = task.GetAwaiter();
                string s = awaiter.GetResult();

                if (task.IsFaulted)
                {
                    Console.Out.WriteLine("faulted");
                }
                return s;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}