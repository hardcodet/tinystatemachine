﻿using System;
using System.Data;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_entry_listener_throws_exception : LooseStateMachineTestBase
    {
        private Action<TransitionErrorData<int>> transitionAction = null;
        private TransitionErrorData<int> errorData;
            
        [SetUp]
        public void Init()
        {
            transitionAction = d => { };
            errorData = null;

            StateMachine.Configuration.ConfigureState(123)
                .OnEntry(ti => { throw new DivideByZeroException(); });

            StateMachine.Configuration.TransitionError += (ed) =>
            {
                errorData = ed;
                transitionAction(ed);
            };
        }

        [Test]
        public async void Exception_listener_should_be_invoked()
        {
            var task = StateMachine.GoToStateAsync(123);
            try
            {
                await task;
                Assert.Fail("No exception");
            }
            catch (AggregateException e)
            {
                e.Flatten().InnerException.Should().BeOfType<DivideByZeroException>();
            }

            errorData.Should().NotBe(null);
            errorData.Exception.Should().BeOfType<DivideByZeroException>();
        }

        [Test]
        public void Transition_task_should_be_marked_as_failed()
        {
            var task = StateMachine.GoToStateAsync(123);
            task.WaitWithoutThrow();
            task.IsFaulted.Should().BeTrue();
            task.Exception.Flatten().InnerException.Should().BeOfType<DivideByZeroException>();

        }

        [Test]
        public void Failure_after_internal_state_change_should_be_indicated()
        {
            StateMachine.GoToStateAsync(123).WaitWithoutThrow();

            errorData.Should().NotBe(null);
            errorData.StateSwitchPerformed.Should().BeTrue();
        }

        [Test]
        public void Failure_before_internal_state_change_should_be_indicated()
        {
            var sm = TestHelper.CreateStateMachine(1, strict: true);

            bool invoked = false;
            sm.Configuration.TransitionError += d =>
            {
                invoked = true;
                d.StateSwitchPerformed.Should().BeFalse();
            };

            Task<IStateTransition<int>> task = sm.GoToStateAsync(3);
            task.WaitWithoutThrow();
            invoked.Should().BeTrue();
        }
        
        [Test]
        public void Exception_should_be_thrown_if_no_listener_marks_exception_as_handled()
        {
            bool handlerInvoked = false;
            Exception e = null;
            transitionAction = d =>
            {
                e = d.Exception;
                handlerInvoked = true;
            };

            var task = StateMachine.GoToStateAsync(123);
            task.WaitWithoutThrow();

            handlerInvoked.Should().BeTrue();
            e.Should().NotBeNull();
            e.Should().BeOfType<DivideByZeroException>();
        }


        [Test]
        public void Navigation_task_should_have_completed_before_invocation()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }


        [Test]
        public void Transitioning_into_a_new_state_should_work()
        {
            transitionAction = async d =>
            {
                await d.StateMachine.GoToStateAsync(5);
                StateMachine.CurrentState.Value.Should().Be(5);
            };

            var task = StateMachine.GoToStateAsync(123).WaitWithoutThrow();
            Task.Delay(1000).Wait();
            StateMachine.CurrentState.Value.Should().Be(5);
        }

        [Test]
        public void State_should_be_marked_as_erroneous()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }
    }
}