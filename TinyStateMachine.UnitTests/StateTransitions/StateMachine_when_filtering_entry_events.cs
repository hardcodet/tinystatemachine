using System.Collections;
using System.Security.Cryptography.X509Certificates;
using FluentAssertions;
using Microsoft.CSharp;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_filtering_entry_events : LooseStateMachineTestBase
    {
        [SetUp]
        public void Init()
        {
            Transition = null;
            feelingGenerous = false;

            StateMachine.Configuration.ConfigureState(999)
                .OnEntry(ti => Transition = ti, t => feelingGenerous);
        }

        public IStateTransition<int> Transition { get; set; }
        private bool feelingGenerous = false;

        [Test]
        public void Matching_events_should_pass()
        {
            feelingGenerous = true;
            StateMachine.GoToStateAsync(999).Wait();
            Transition.Should().NotBeNull();
            Transition.NewState.Value.Should().Be(999);
        }

        [Test]
        public void Nonmachtching_events_should_be_skipped()
        {
            feelingGenerous = false;
            StateMachine.GoToStateAsync(999).Wait();
            Transition.Should().BeNull();
        }
    }
}