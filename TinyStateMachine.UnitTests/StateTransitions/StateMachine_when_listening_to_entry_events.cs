using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_listening_to_entry_events : LooseStateMachineTestBase
    {
        private List<int> states;

        [SetUp]
        public void Init()
        {
            states = new List<int>();
        }

        [Test]
        public void Entry_listener_should_be_invoked()
        {
            StateMachine.Configuration.ConfigureState(1)
                .OnEntry(ti => states.Add(ti.NewState.Value));

            StateMachine.GoToStateAsync(1).Wait();
            states.Single().Should().Be(1);
        }

        [Test]
        public void Filtering_from_specified_source_state_should_only_trigger_if_source_matches()
        {
            //from 1 -> 2
            StateMachine.Configuration.ConfigureState(2)
                .OnEntryFrom(1, ti => states.Add(ti.NewState.Value));

            //enter 2
            StateMachine.GoToStateAsync(2).Wait();
            states.Should().BeEmpty();

            //2 -> 1: nothing
            StateMachine.GoToStateAsync(1).Wait();
            states.Should().BeEmpty();

            //1 -> 2: listener
            StateMachine.GoToStateAsync(2).Wait();
            states.Single().Should().Be(2);
        }
    }
}