﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_listening_to_parameterized_entry_events : LooseStateMachineTestBase
    {
        [Test]
        public void Parameter_should_be_submitted_to_listener()
        {
            bool invoked = false;

            StateMachine.Configuration.ConfigureState(124)
                .OnEntryWithParameter<string>((ti, s) =>
                {
                    invoked = true;
                    s.Should().Be("hello");
                });

            StateMachine.GoToStateAsync(124, "hello").Wait();
            invoked.Should().BeTrue();
        }

        [Test]
        public void Subtype_injection_should_work()
        {
            bool invoked = false;

            var exception = new ArgumentOutOfRangeException();

            StateMachine.Configuration.ConfigureState(124)
                .OnEntryWithParameter<ArgumentException>((ti, e) =>
                {
                    invoked = true;
                    e.Should().BeSameAs(exception);
                });

            StateMachine.GoToStateAsync(124, exception).Wait();
            invoked.Should().BeTrue();
        }

        [Test]
        public void Base_types_should_not_cause_invocation()
        {
            bool invoked = false;

            var exception = new ArgumentException();

            StateMachine.Configuration.ConfigureState(124)
                .OnEntryWithParameter<ArgumentOutOfRangeException>((ti, e) =>
                {
                    invoked = true;
                });

            StateMachine.GoToStateAsync(124, exception).Wait();
            invoked.Should().BeFalse();
        }

        [Test]
        public void Different_types_should_not_cause_invocation()
        {
            bool invoked = false;

            StateMachine.Configuration.ConfigureState(124)
                .OnEntryWithParameter<bool>((ti, s) =>
                {
                    invoked = true;
                });

            StateMachine.GoToStateAsync(124, "hello").Wait();
            invoked.Should().BeFalse();
        }

        [Test]
        public void Null_parameters_should_work_and_indicate_parameter_availability_on_nullable_types()
        {
            bool invoked = false;

            StateMachine.Configuration.ConfigureState(124)
                .OnEntryWithParameter<Exception>((ti, e) =>
                {
                    invoked = true;
                    ti.NewState.Parameter.HasValue.Should().BeTrue();
                    e.Should().BeNull();
                });

            StateMachine.GoToStateAsync(124, null).Wait();
            invoked.Should().BeTrue();
        }

        [Test]
        public void Null_parameters_should_not_trigger_invocation_on_value_types()
        {
            bool invoked = false;

            StateMachine.Configuration.ConfigureState(124)
                .OnEntryWithParameter<bool>((ti, e) =>
                {
                    invoked = true;
                });

            StateMachine.GoToStateAsync(124, null).Wait();
            invoked.Should().BeFalse();
        }
    }
}