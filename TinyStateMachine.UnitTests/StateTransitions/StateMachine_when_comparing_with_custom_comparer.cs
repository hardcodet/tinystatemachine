using System;
using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateTransitions
{
    [TestFixture]
    public class StateMachine_when_comparing_with_custom_comparer
    {
        public class UserComparer : IEqualityComparer<User>
        {
            public bool CompareByName { get; set; }

            public UserComparer(bool compareByName)
            {
                CompareByName = compareByName;
            }

            public bool Equals(User x, User y)
            {
                return CompareByName ? x.Name == y.Name : x.Id == y.Id;
            }

            public int GetHashCode(User user)
            {
                return CompareByName ? user.Name.GetHashCode() : user.Id.GetHashCode();
            }
        }

        public UserComparer Comparer { get; set; }
        public StateMachine<User> StateMachine { get; set; }
        public User Fred { get; set; }
        public User Anne { get; set; }

        [SetUp]
        public void Init()
        {
            Comparer = new UserComparer(true);
            Anne = User.Anne();
            Fred = User.Fred();

            var config = TestHelper.CreateStateMachineConfiguration(true, Comparer);

            config.ConfigureState(User.Fred());
            config.ConfigureState(User.Anne());

            StateMachine = new StateMachine<User>(config, User.Fred());
        }

        [Test]
        public void Comparer_should_be_applied()
        {
            var noAnne = new User(Anne.Id + 1, Anne.Name);
            Action a = () => StateMachine.GoToStateAsync(noAnne).Wait();

            //since we're comparing names, that should be ok
            Comparer.CompareByName = true;

            a.ShouldNotThrow();
            StateMachine.CurrentState.Value.Should().BeSameAs(noAnne);

            StateMachine.GoToStateAsync(Fred).Wait();
            
            Comparer.CompareByName = false;
            a.ShouldThrow<InvalidOperationException>();
        }

        [Test]
        public void Case_insensitive_comparison_should_be_possible()
        {
            bool fooInvoked = false;

            var comparer = StringComparer.OrdinalIgnoreCase;
            var config = TestHelper.CreateStateMachineConfiguration(true, comparer);

            config.ConfigureState("start");
            config.ConfigureState("foo").OnEntry(ti => fooInvoked = true);
            
            var stateMachine = new StateMachine<string>(config, "START");
            stateMachine.GoToStateAsync("FOO").Wait();
            fooInvoked.Should().BeTrue();
        }
    }
}