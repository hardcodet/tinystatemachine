﻿using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests
{
    [TestFixture]
    public class StateMachine_when_creating
    {
        [SetUp]
        public void Init()
        {
            //TODO initialization code running for each test
            Assert.Inconclusive("Initialization code not implemented yet.");
        }


        [Test]
        public void Initial_state_should_be_assigned()
        {
            var sm = TestHelper.CreateStateMachine("start");
            sm.CurrentState.Should().NotBeNull();
            sm.CurrentState.Value.Should().Be("start");
        }


        [Test]
        public void Initializing_with_a_null_value_should_be_valid()
        {
            //TODO we may still prevent null values by means of configuration I guess
            var sm = TestHelper.CreateStateMachine<string>(null);
            sm.CurrentState.Should().NotBeNull();
            sm.CurrentState.Value.Should().BeNull();
        }

    }
}



