﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using TinyStateMachine.Util;

namespace TinyStateMachine.UnitTests.StateInfos
{
    [TestFixture]
    public class State_when_setting_or_accessing_parameter
    {
        internal State<int> State { get; private set; }

        [SetUp]
        public void Init()
        {
            var configuration = new StateConfiguration<int>(33, "test", EqualityComparer<int>.Default);
            State = new State<int>(33, configuration);
        }

        [Test]
        public void Parameter_availability_should_be_indicated()
        {
            State.SetParameter("hello");
            State.Parameter.HasValue.Should().BeTrue();
        }

        [Test]
        public void Parameter_value_should_be_stored()
        {
            State.SetParameter("hello");
            State.Parameter.Value.Should().Be("hello");
        }

        [Test]
        public void Assigning_null_value_should_be_possible()
        {
            State.SetParameter(null);
            State.Parameter.HasValue.Should().BeTrue();
            State.Parameter.Value.Should().BeNull();
        }

        [Test]
        public void Parameter_should_not_be_set_by_default()
        {
            State.Parameter.HasValue.Should().BeFalse();
        }
    }
}