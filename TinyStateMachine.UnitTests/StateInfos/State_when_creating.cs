﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateInfos
{
    [TestFixture]
    public class State_when_creating
    {
        internal State<int> State { get; private set; }

        [SetUp]
        public void Init()
        {
            var configuration = new StateConfiguration<int>(33, "test", EqualityComparer<int>.Default);
            State = new State<int>(33, configuration);
        }


        [Test]
        public void Value_should_be_assigned()
        {
            State.Value.Should().Be(33);
        }

        [Test]
        public void Configuration_should_be_stored()
        {
            State.Configuration.Should().NotBeNull();
        }
    }
}



