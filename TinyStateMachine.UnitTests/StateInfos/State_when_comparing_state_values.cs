﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.StateInfos
{
    [TestFixture]
    public class State_when_comparing_state_values
    {
        [Test]
        public void Comparison_with_other_StateInfo_should_compare_state_values()
        {
            var s1 = TestHelper.CreateState("Foo");
            var s2 = TestHelper.CreateState("Foo");
            var s3 = TestHelper.CreateState("Bar");

            s1.Should().Be(s2);
            s1.Should().NotBe(s3);
        }

        [Test]
        public void Null_value_comparison_should_work()
        {
            var s1 = TestHelper.CreateState<string>(null);
            var s2 = TestHelper.CreateState<string>(null);
            var s3 = TestHelper.CreateState("Foo");

            s1.Should().Be(s2);
            s1.Should().NotBe(s3);
        }
    }
}



