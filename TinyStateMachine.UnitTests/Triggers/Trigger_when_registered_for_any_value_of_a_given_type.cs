﻿using System;
using System.Collections.Generic;
using System.Linq;

using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.Triggers
{
    [TestFixture]
    public class StateMachine_when_resolving_triggers : StrictStateMachineTestBase
    {
        [Test]
        public void Multiple_matches_should_cause_exception()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }

        [Test]
        public void No_matches_should_cause_exception()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }

        [Test]
        public void Resolving_single_trigger_should_fire_the_trigger()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }
    }

    [TestFixture]
    public class StateMachine_when_firing_triggers : StrictStateMachineTestBase
    {
        [Test]
        public void Trigger_value_should_be_submitted()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }

        [Test]
        public void State_should_change_to_state_that_was_related_to_trigger()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }
    }

    [TestFixture]
    public class Triggers_when_firing : StrictStateMachineTestBase
    {
        public int State { get; set; }
        public object Trigger { get; set; }

        [SetUp]
        public void Init()
        {
            State = -1;
            Trigger = null;

            StateMachine.Configuration.ConfigureState(1)
                .IsTriggeredByValue("one");

            StateMachine.Configuration.ConfigureState(2)
                .IsTriggeredByValues("two", "deux", "due");

            StateMachine.Configuration.ConfigureState(3)
                .IsTriggeredByAny<bool>();

            StateMachine.Configuration.ConfigureState(4)
                .IsTriggeredBy<string>(s => s == "four" || s == "vier");

            StateMachine.StateTransition += ti =>
            {
                State = ti.StateMachine.CurrentState.Value;
                Trigger = ti.NewState.Parameter.Value;
            };
        }


        [Test]
        public void Invoking_single_value_trigger_should_work()
        {
            StateMachine.TriggerAsync("one").Wait();

            State.Should().Be(1);
            Trigger.Should().Be("one");
        }

        [Test]
        public void Invoking_one_of_multiple_values_trigger_should_work()
        {
            StateMachine.TriggerAsync("two").Wait();
            State.Should().Be(2);
            Trigger.Should().Be("two");

            State = -1;

            StateMachine.TriggerAsync("deux").Wait();
            State.Should().Be(2);
            Trigger.Should().Be("deux");
        }

        [Test]
        public void Invoking_type_based_trigger_should_work()
        {
            StateMachine.TriggerAsync(true).Wait();
            State.Should().Be(3);
            Trigger.Should().Be(true);

            State = -1;

            StateMachine.TriggerAsync(false).Wait();
            State.Should().Be(3);
            Trigger.Should().Be(false);
        }

        [Test]
        public void Invoking_predicate_trigger_should_work()
        {
            StateMachine.TriggerAsync("four").Wait();
            State.Should().Be(4);
            Trigger.Should().Be("four");

            State = -1;

            StateMachine.TriggerAsync("vier").Wait();
            State.Should().Be(4);
            Trigger.Should().Be("vier");
        }
    }



    [TestFixture]
    public class Trigger_when_registered_for_any_value_of_a_given_type : StrictStateMachineTestBase
    {
        public User Fred { get; set; }
        public User CallbackUser { get; set; }

        [SetUp]
        public void Init()
        {
            StateMachine.Configuration.ConfigureState(123)
                .IsTriggeredByAny<User>((ti, u) => CallbackUser = u);

            Fred = User.Fred();
            CallbackUser = null;
        }

        [Test]
        public void Trigger_should_be_fired_on_types()
        {
            IStateTransition<int> transition = StateMachine.TriggerAsync(Fred).Result;

            StateMachine.CurrentState.Value.Should().Be(123);
            transition.NewState.Parameter.Value.Should().BeSameAs(Fred);
            CallbackUser.Should().NotBeNull();
            CallbackUser.Should().BeSameAs(Fred);
        }

        [Test]
        public void Null_values_should_be_supported()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }

        [Test]
        public void Derived_types_should_be_supported()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }

        [Test]
        public void Other_triggers_should_be_ignored()
        {
            Assert.Inconclusive("Test not implemented yet.");
        }
    }


    
}



