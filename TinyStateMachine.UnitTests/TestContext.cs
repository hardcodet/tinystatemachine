﻿namespace TinyStateMachine.UnitTests
{
    public class TestContext
    {
        public User User { get; set; }

        public TestContext(User user = null)
        {
            User = user ?? User.Anne();
        }
    }
}