﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyStateMachine.UnitTests
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public User(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public static User Fred()
        {
            return new User(1, "Fred");
        }

        public static User Anne()
        {
            return new User(2, "Anne");
        }
    }
}
