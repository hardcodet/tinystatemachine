using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.Previews
{
    [TestFixture]
    public class StateMachine_when_listening_to_preview_events : LooseStateMachineTestBase
    {
        public IStateTransition<int> Transition { get; set; }
        private bool allowEntry;
        private bool allowExit;
        private bool transitionCompleted;

        [SetUp]
        public void Init()
        {
            Transition = null;
            allowEntry = true;
            allowExit = true;
            transitionCompleted = false;


            StateMachine.Configuration.ConfigureState(1)
                .PreviewOnExit(ti =>
                {
                    Transition = ti;
                    return allowExit;
                });

            StateMachine.Configuration.ConfigureState(1)
                .PreviewOnEntry(ti =>
                {
                    Transition = ti;
                    return allowEntry;
                });

            StateMachine.StateTransition += ti => transitionCompleted = true;
            StateMachine.GoToStateAsync(0).Wait();
        }

        [Test]
        public async void Submitted_transition_object_should_contain_current_and_next_state()
        {
            var task = StateMachine.GoToStateAsync(1);
            await task;

            Transition.PreviousState.Value.Should().Be(0);
            Transition.NewState.Value.Should().Be(1);
        }

        [Test]
        public async void Entry_preview_should_fire_before_transition()
        {
            transitionCompleted = false;
            StateMachine.Configuration.ConfigureState(1)
                .PreviewOnEntry(ti =>
                {
                    transitionCompleted.Should().BeFalse();
                    return true;
                });

            await StateMachine.GoToStateAsync(1);
        }

        [Test]
        public async void Exit_preview_should_fire_before_transition()
        {
            transitionCompleted = false;
            StateMachine.Configuration.ConfigureState(0)
                .PreviewOnExit(ti =>
                {
                    transitionCompleted.Should().BeFalse();
                    return true;
                });

            await StateMachine.GoToStateAsync(1);
        }

        [Test]
        public async void Entry_preview_should_not_fire_on_exit()
        {
            bool invoked = false;
            StateMachine.Configuration.ConfigureState(0)
                .PreviewOnEntry(ti =>
                {
                    invoked = true;
                    return true;
                });

            await StateMachine.GoToStateAsync(1);

            invoked.Should().BeFalse();
        }

        [Test]
        public async void Exit_preview_should_not_fire_on_entry()
        {
            bool invoked = false;
            StateMachine.Configuration.ConfigureState(1)
                .PreviewOnExit(ti =>
                {
                    invoked = true;
                    return true;
                });

            await StateMachine.GoToStateAsync(1);

            invoked.Should().BeFalse();
        }
    }
}