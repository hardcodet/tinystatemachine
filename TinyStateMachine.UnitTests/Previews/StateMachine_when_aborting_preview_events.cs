using System;
using FluentAssertions;
using NUnit.Framework;

namespace TinyStateMachine.UnitTests.Previews
{
    [TestFixture]
    public class StateMachine_when_aborting_preview_events : LooseStateMachineTestBase
    {
        public IStateTransition<int> Transition { get; set; }
        private bool allowEntry;
        private bool allowExit;

        [SetUp]
        public void Init()
        {
            Transition = null;
            allowEntry = true;
            allowExit = true;

            StateMachine.Configuration.ConfigureState(1)
                .PreviewOnEntry(ti => allowEntry)
                .PreviewOnExit(ti => allowExit);

            StateMachine.GoToStateAsync(0).Wait();
        }


        [Test]
        public async void Entry_preview_should_prevent_state_change()
        {
            allowEntry = false;

            var task = StateMachine.GoToStateAsync(1);
            try
            {
                await task;
                Assert.Fail("no exception");
            }
            catch (Exception e)
            {
                task.IsCanceled.Should().BeTrue();
                StateMachine.CurrentState.Value.Should().Be(0);
            }
        }

        [Test]
        public async void Exit_preview_should_prevent_state_change()
        {
            allowExit = false;

            var task = StateMachine.GoToStateAsync(1);
            await task;
            task.IsCanceled.Should().BeFalse();

            task = StateMachine.GoToStateAsync(2);
            try
            {
                await task;
                Assert.Fail("no exception");
            }
            catch (Exception e)
            {
                task.IsCanceled.Should().BeTrue();
                StateMachine.CurrentState.Value.Should().Be(1);
            }
        }

        [Test]
        public async void Subsequent_handlers_should_not_be_invoked()
        {
            bool first = false, second = false, third = false;

            StateMachine.Configuration.ConfigureState(10)
                .PreviewOnEntry(ti => first = true)
                .PreviewOnEntry(ti =>
                {
                    second = true;
                    return false;
                })
                .PreviewOnEntry(fi => third = true);


            var task = StateMachine.GoToStateAsync(10);
            try
            {
                await task;
                Assert.Fail("no exception");
            }
            catch (Exception e)
            {
                task.IsCanceled.Should().BeTrue();
                first.Should().BeTrue();
                second.Should().BeTrue();
                third.Should().BeFalse();
            }


        }

        [Test]
        public void Preventing_initial_state_should_throw_exception_on_state_machine_construction()
        {
            var mc = TestHelper.CreateStateMachineConfiguration<string>();
            mc.ConfigureState("initial").PreviewOnEntry(ti => false);

            Action a = () => new StateMachine<string>(mc, "initial");
            a.ShouldThrow<ArgumentException>();

        }
    }
}