using NUnit.Framework;

namespace TinyStateMachine.UnitTests
{
    public abstract class LooseStateMachineTestBase
    {
        internal StateMachine<int> StateMachine { get; set; }

        [SetUp]
        public void BaseInit()
        {
            StateMachine = TestHelper.CreateStateMachine(12345, strict:false);
        }
    }

    public abstract class StrictStateMachineTestBase
    {
        internal StateMachine<int> StateMachine { get; set; }

        [SetUp]
        public void BaseInit()
        {
            StateMachine = TestHelper.CreateStateMachine(12345, strict: true);
        }
    }
}