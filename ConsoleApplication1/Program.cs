﻿using System;
using System.Threading.Tasks;
using TinyStateMachine;
using TinyStateMachine.Execution;
using TinyStateMachine.Rules;

namespace ConsoleApplication1
{

    class Program
    {
        static void Main(string[] args)
        {
            RunStates();
        }

        static async void RunStates()
        {
            //non-strict state machine - allows transitions to arbitrary states without configuring them
            var smConfiguration = new StateMachineConfiguration<string>(strict: false);
            IStateMachine<string> stateMachine = new StateMachine<string>(smConfiguration, "start");
            stateMachine.StateTransition += OnTransition;

            smConfiguration.ConfigureState("foo")
                .OnEntry(DoSomethingAndContinue);

            //configure a state in order to support triggers, and also register an entry listener
            smConfiguration.ConfigureState("end")
                .IsTriggeredBy<int>(o => o < 0)
                .OnEntry(ti => Console.WriteLine("THIS IS THE END"))
                .OnEntry(DoSomethingAndContinue)
                .PreviewOnEntry(ti => true)
                .PreviewOnExit(ti => true);

            smConfiguration.ConfigureState("async")
                .ExecuteAndContinue(OnAsync, (ts) => { })
                .ExecuteAndContinue(OnAsync2, (ts, val) => { Console.WriteLine(val); });
                
                

            //do state changes
            await stateMachine.GoToStateAsync("step1");
            //change state with a parameter
            await stateMachine.GoToStateAsync("step2", 99.9999);
            //do state change based on trigger
            try
            {
                await stateMachine.TriggerAsync(-1);
            }
            catch (TaskCanceledException e)
            {
            }
            await stateMachine.GoToStateAsync("start");
            Console.Out.WriteLine("WHAT");
            await stateMachine.GoToStateAsync("async");
        }

        private static Task<int> OnAsync2(IStateTransition<string> arg)
        {
            return Task.FromResult(3);
        }

        private static Task OnAsync(IStateTransition<string> arg)
        {
            Console.Out.WriteLine("delaying...");
            return Task.Delay(3000);
        }

        private static async void DoSomethingAndContinue(IStateTransition<string> transition)
        {
            //can also be written as: int result = await Task.Run(...)
            Task<int> task = Task.Run(() => new Random().Next(4));
            int result = await task;

            if (result % 2 == 1)
            {
                transition.StateMachine.GoToStateAsync("Odd");
            }
            else
            {
                transition.StateMachine.GoToStateAsync("Even");
            }
        }
       
        private static void OnTransition(IStateTransition<string> transition)
        {
            Console.Out.WriteLine("Transitioned from {0} to {1} with parameter/trigger {2}", transition.PreviousState.Value, transition.NewState.Value, transition.NewState.Parameter.TryGetValue<object>() ?? " - ");
        }
    }
}
