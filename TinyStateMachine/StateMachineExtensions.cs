﻿using System;
using System.Collections.Generic;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Extension methods that extend the capabilities of state machine configurations.
    /// </summary>
    public static class StateMachineExtensions
    {
        /// <summary>
        /// Registers a listener action which is being notified upon any successful state
        /// transition of the state machine from one particular state to another.
        /// </summary>
        /// <param name="stateMachine">State machine being setup.</param>
        /// <param name="from">The originating state of the state machine.</param>
        /// <param name="to">The new state of the state machine.</param>
        /// <param name="listener">A callback listener that is being invoked on every matching state
        /// change.</param>
        /// <returns>The state machine configuration itself in order to allow chainging.</returns>
        public static IStateMachine<TState> OnTransition<TState>(this IStateMachine<TState> stateMachine, TState from,
            TState to, Action<IStateTransition<TState>>  listener)
        {
            Guard.NotNull(stateMachine, () => stateMachine);

            IEqualityComparer<TState> comparer;
            var sm = stateMachine as StateMachine<TState>;
            comparer = sm == null ? EqualityComparer<TState>.Default : sm.Configuration.EqualityComparer;
            
            stateMachine.StateTransition += t =>
            {
                if (comparer.Equals(from, t.PreviousState.Value) && comparer.Equals(to, t.NewState.Value))
                {
                    listener(t);
                }
            };

            return stateMachine;
        }
    }
}
