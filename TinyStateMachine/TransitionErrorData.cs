using System;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Encapsulates information about an exception that occurred during a
    /// state transition.
    /// </summary>
    public class TransitionErrorData<TState>
    {
        /// <summary>
        /// The state machine that processed the transition.
        /// </summary>
        public IStateMachine<TState> StateMachine { get; internal set; }

        /// <summary>
        /// The exception that occurred.
        /// </summary>
        public Exception Exception { get; internal set; }

        /// <summary>
        /// Indicates whether the internal state has already switched over or not.
        /// </summary>
        public bool StateSwitchPerformed { get; internal set; }

        /// <summary>
        /// The original state. If <see cref="StateSwitchPerformed"/> is false, this
        /// is still corresponds <see cref="IStateMachine{TState}.CurrentState"/>.
        /// </summary>
        public TState FromState { get; internal set; }

        /// <summary>
        /// The requested destination state. If <see cref="StateSwitchPerformed"/> is true, this
        /// is the new state as reflected by <see cref="IStateMachine{TState}.CurrentState"/>.
        /// </summary>
        public TState ToState { get; internal set; }

        /// <summary>
        /// Represents the transition parameter.
        /// </summary>
        public TransitionParameter TransitionParameter { get; internal set; }
    }
}