﻿using System.Threading.Tasks;

namespace TinyStateMachine
{
    /// <summary>
    /// Encapsulats a command that is being invoked on a trigger on a given state.
    /// </summary>
    public interface ITriggerCommand<TState>
    {
        /// <summary>
        /// Executes the underlying command and submits the trigger that
        /// caused the change. 
        /// </summary>
        /// <param name="stateMachine"></param>
        /// <param name="state"></param>
        /// <param name="triggerValue"></param>
        Task Execute(IStateMachine<TState> stateMachine, IState<TState> state, object triggerValue);
    }
}