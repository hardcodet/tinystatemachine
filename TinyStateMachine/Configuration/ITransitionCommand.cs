using System.Threading.Tasks;

namespace TinyStateMachine
{
    /// <summary>
    /// Encapuslates a command that executes on a given transition
    /// (e.g. OnEntry, or on a given trigger).
    /// </summary>
    public interface ITransitionCommand<TState>
    {
        /// <summary>
        /// Evaluates and executes the underlying command.
        /// </summary>
        Task Execute(IStateTransition<TState> transition);
    }
}