﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Encapsulates configuration for a given trigger condition that causes a transition
    /// into a given state.
    /// </summary>
    internal class TriggerListener<TState>
    {
        /// <summary>
        /// A predicate function that determines whether the underlying commands should be invoked for the trigger
        /// or not.
        /// </summary>
        public Func<object, bool> Predicate { get; private set; }

        /// <summary>
        /// Commands that are being executed if the trigger fires. May execute actions, invoke
        /// state transitions etc.
        /// </summary>
        public List<ITriggerCommand<TState>>  TriggerCommands { get; private set; }

        /// <summary>
        /// Creates the trigger configuration.
        /// </summary>
        public TriggerListener(Func<object, bool> predicate)
        {
            Predicate = Guard.NotNull(predicate, () => predicate);
            TriggerCommands = new List<ITriggerCommand<TState>>();
        }

        public async Task Execute(IStateMachine<TState> stateMachine, IState<TState> state, object triggerValue)
        {
            foreach (var triggerCommand in TriggerCommands)
            {
                await triggerCommand.Execute(stateMachine, state, triggerValue);
            }
        }
    }
}