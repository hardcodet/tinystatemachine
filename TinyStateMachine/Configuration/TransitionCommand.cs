﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TinyStateMachine
{
    public class TransitionCommand<TState> : ITransitionCommand<TState>
    {
        public Func<IStateTransition<TState>, Task> TransitionAction { get; set; }

        /// <summary>
        /// A predicate that can be used to filter execution of registered commands.
        /// </summary>
        public Func<IStateTransition<TState>, bool> Predicate { get; set; }

        /// <summary>
        /// Evaluates and executes the underlying command.
        /// </summary>
        public async Task Execute(IStateTransition<TState> transition)
        {
            if (Predicate == null || Predicate(transition))
            {
                await TransitionAction(transition);
            }
        }
    }

    public class TransitionCommand<TState, TResult> : ITransitionCommand<TState>
    {
        public Func<IStateTransition<TState>, Task<TResult>> CommandFunc { get; set; }

        /// <summary>
        /// A predicate that can be used to filter execution of registered commands.
        /// </summary>
        public Func<IStateTransition<TState>, bool> Predicate { get; set; }

        public List<Func<IStateTransition<TState>, TResult, Task>> FollowUpCommands { get; set; }

        public async Task Execute(IStateTransition<TState> transition)
        {
            if (CommandFunc == null) return;

            if (Predicate != null && !Predicate(transition))
            {
                return;
            }

            TResult result = await CommandFunc(transition);
            
            foreach (var cmd in FollowUpCommands)
            {
                await cmd(transition, result);
            }
        }

        public TransitionCommand()
        {
            FollowUpCommands = new List<Func<IStateTransition<TState>, TResult, Task>>();
        }
    }
}
