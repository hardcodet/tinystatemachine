using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TinyStateMachine
{
    /// <summary>
    /// Encapsulates a listener delegate that is being notified on transition events that
    /// match certain conditions, which are evaluated by a <see cref="Predicate"/>.
    /// </summary>
    internal class TransitionListener<TState>
    {
        /// <summary>
        /// A predicate function that determines whether the listener should be invoked
        /// for the actual transition. Used in order to enable filtered listener scenarios
        /// baked into the API or extension methods.
        /// </summary>
        public Func<IStateTransition<TState>, bool> Predicate { get; set; }

        /// <summary>
        /// Commands that are being executed if the transition listener is being activated.
        /// May execute actions, enqueue other state transitions etc.
        /// </summary>
        public List<ITransitionCommand<TState>> TransitionCommands { get; private set; }

        public TransitionListener(Func<IStateTransition<TState>, bool> predicate = null)
        {
            TransitionCommands = new List<ITransitionCommand<TState>>();
            Predicate = predicate;
        }
        
        public async Task Execute(IStateTransition<TState> transition)
        {
            foreach (var transitionCommand in TransitionCommands)
            {
                await transitionCommand.Execute(transition);
            }
        }
    }
}