using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TinyStateMachine
{
    public class TriggerCommand<TState, TTriggerValue> : ITriggerCommand<TState>
    {
        public Func<ITriggerData<TState, TTriggerValue>, Task> TriggerAction { get; set; }

        public async Task Execute(IStateMachine<TState> stateMachine, IState<TState> state, object triggerValue)
        {
            var triggerData = new TriggerData<TState, TTriggerValue>(stateMachine, state, (TTriggerValue)triggerValue);
            if (TriggerAction != null) await TriggerAction(triggerData);
        }
    }



    /// <summary>
    /// A trigger command that retrieves a given value through a func, and then invokes other actions.
    /// </summary>
    public class TriggerCommand<TState, TTriggerValue, TResult> : ITriggerCommand<TState>
    {
        public Func<ITriggerData<TState, TTriggerValue>, Task<TResult>> TriggerFunc { get; set; }

        public List<Func<ITriggerData<TState, TTriggerValue>, TResult, Task>> FollowUpCommands { get; set; }

        public TriggerCommand()
        {
            FollowUpCommands = new List<Func<ITriggerData<TState, TTriggerValue>, TResult, Task>>();
        }

        /// <summary>
        /// Executes the underlying command and submits the trigger that
        /// caused the change. 
        /// </summary>
        /// <param name="stateMachine"></param>
        /// <param name="state"></param>
        /// <param name="triggerValue"></param>
        public async Task Execute(IStateMachine<TState> stateMachine, IState<TState> state, object triggerValue)
        {
            if (TriggerFunc == null) return;

            var triggerData = new TriggerData<TState, TTriggerValue>(stateMachine, state, (TTriggerValue)triggerValue);
            TResult result = await TriggerFunc(triggerData);
            foreach (var cmd in FollowUpCommands)
            {
                await cmd(triggerData, result);
            }
        }
    }
}