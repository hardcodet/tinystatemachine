using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// A running instance of a state machine.
    /// </summary>
    /// <typeparam name="TState">The type of the machine's states. Could be strings, enums,
    /// or any other object / interface.</typeparam>
    public class StateMachine<TState> : IStateMachine<TState>
    {
        private State<TState> currentState;

        internal StateMachineConfiguration<TState> Configuration { get; private set; }

        /// <summary>
        /// The current state.
        /// </summary>
        public IState<TState> CurrentState
        {
            get { return currentState; }
        }

        /// <summary>
        /// Maintains enqueued transition requests.
        /// </summary>
        internal TaskQueue TransitionQueue { get; private set; }


        //TODO Status: running, canceled, error

        /// <summary>
        /// TODO allow additional event previews for validation hooks, transformations etc
        /// An event that fires upon any successful state transition of the state machine.
        /// </summary>
        public event Action<IStateTransition<TState>> StateTransition;

        /// <summary>
        /// Creates the state machine based on a configuration object, along with its initial state.
        /// </summary>
        /// <param name="configuration">Configures the state machine's states and behavior.</param>
        /// <param name="initialState">The initial state value that marks the starting point.</param>
        public StateMachine(StateMachineConfiguration<TState> configuration, TState initialState)
        {
            Configuration = Guard.NotNull(configuration, () => configuration);
            TransitionQueue = new TaskQueue();
            Parameters = new ParameterDictionary();

            Task<IStateTransition<TState>> transitionTask = PerformTransition(initialState, Nothing.Instance);
            if (transitionTask.Result == null)
            {
                string msg = "The transition to the initial state {0} has been aborted.";
                msg = String.Format(msg, initialState);
                throw new ArgumentException(msg);
            }
        }

        /// <summary>
        /// Creates a minimalistic, non-strict state machine that allows arbitrary
        /// states that match the state types.
        /// </summary>
        /// <param name="initialState">The initial state value that marks the starting point.</param>
        public StateMachine(TState initialState)
            : this(new StateMachineConfiguration<TState>(false), initialState)
        {
        }

        public ParameterDictionary Parameters { get; private set; }

        /// <summary>
        /// Enqueues the transition into a different state.
        /// </summary>
        /// <param name="state">The state to transition to.</param>
        /// <returns>A task that indicates the completion of the state transition.</returns>
        public Task<IStateTransition<TState>> GoToStateAsync(TState state)
        {
            return GoToStateAsync(state, Nothing.Instance);
        }

        /// <summary>
        /// Enqueues the transition into a different state with a
        /// complementary parameter value.
        /// </summary>
        /// <param name="state">The state to transition to.</param>
        /// <param name="parameter">An optional payload that is being stored with
        /// the transition.</param>
        /// <returns>A task that indicates the completion of the state transition.</returns>
        public Task<IStateTransition<TState>> GoToStateAsync(TState state, object parameter)
        {
            var transitionTask = TransitionQueue.EnqueueTask(() => PerformTransition(state, parameter));
            return CreateTransitionTask(transitionTask);
        }

        private static Task<IStateTransition<TState>> CreateTransitionTask(Task<IStateTransition<TState>> transitionTask)
        {
            var tcs = new TaskCompletionSource<IStateTransition<TState>>();
            transitionTask.ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    tcs.SetException(t.Exception);
                    return;
                }
                if (t.IsCanceled)
                {
                    tcs.SetCanceled();
                    return;
                }

                var result = t.Result;
                if (result == null)
                {
                    tcs.SetCanceled();
                }
                else
                {
                    tcs.SetResult(result);
                }
            });

            return tcs.Task;
        }

        /// <summary>
        /// Triggers a state transition using a trigger value that corresponds to a trigger
        /// condition specified through the state machine configuration.
        /// </summary>
        /// <typeparam name="TTrigger">Trigger value type.</typeparam>
        /// <param name="triggerValue">The value that should cause a state transition. The trigger value will be
        /// attached to the <see cref="IState{TState}.Parameter"/> of the new state.</param>
        /// <returns>A task that indicates the success of the transition along with the actual transition
        /// data.</returns>
        public Task TriggerAsync<TTrigger>(TTrigger triggerValue)
        {
            var task = TransitionQueue.EnqueueTask(async () =>
            {
                //resolve the triggers for the current state
                StateConfiguration<TState> configuration = Configuration.ResolveConfiguration(CurrentState.Value);
                var triggers = configuration.TriggerConfigurations.Where(tc => tc.Predicate(triggerValue)).ToArray();
                
                if (triggers.Length == 0)
                {
                    string msg = "No matching trigger configuration found for trigger [{0}] of type {1} for state {2}.";
                    msg = String.Format(msg, triggerValue, typeof (TTrigger).Name, configuration.ResolveName());
                    throw new ArgumentException(msg, "triggerValue");
                }

                if (triggers.Length > 1)
                {
                    string msg = "Multiple matching trigger configurations found for trigger [{0}] of type {1} for state {2}. There must be exactly one match.";
                    msg = String.Format(msg, triggerValue, typeof(TTrigger).Name, configuration.ResolveName());
                    throw new ArgumentException(msg, "triggerValue");
                }

                TriggerListener<TState> trigger = triggers[0];

                State<TState> state = new State<TState>(configuration.RepresentedState, configuration);
                state.SetParameter(triggerValue);
                
                await trigger.Execute(this, state, triggerValue);
            });

            return task;
        }

        internal async Task<IStateTransition<TState>> PerformTransition(TState state, object parameter, StateConfiguration<TState> configuration = null, Action<StateTransition<TState>> postAction = null)
        {
            State<TState> previous = currentState;
            bool switchPerformed = false;

            try
            {
                StateConfiguration<TState> newStateConfiguration = configuration ?? Configuration.ResolveConfiguration(state);
                var stateInfo = new State<TState>(state, newStateConfiguration);

                if (!(parameter is Nothing))
                {
                    stateInfo.SetParameter(parameter);
                }

                var transition = new StateTransition<TState>(this, previous, stateInfo);

                //for strict state machines, make sure the target state is allowed by transition rules
                if (Configuration.Strict && previous != null)
                {
                    ICollection<TransitionListener<TState>> transitionRules = previous.Configuration.TransitionRules;
                    if (!transitionRules.Any(r => r.Predicate(transition)))
                    {
                        string msg = "There is no transition rule allowing the transition from state {0} to state {1} for this strict state machine";
                        msg = String.Format(msg, previous.Configuration.Name, newStateConfiguration.Name);
                        throw new InvalidOperationException(msg);
                    }
                }

                //TODO fire preview event on previous before exit?
                bool previewStatus = previous == null || EvaluatePreviewListeners(previous.Configuration.ExitPreviewListeners, transition);
                if (!previewStatus) return null;

                previewStatus = EvaluatePreviewListeners(newStateConfiguration.EntryPreviewListeners, transition);
                if (!previewStatus) return null;

                //set the new state - at this point, the state machine has switched to the new state, even
                //if exceptions occur further down the road
                currentState = stateInfo;
                switchPerformed = true;

                //fire exit events on previous state (previous is only null on initial state setting)
                if (previous != null)
                {
                    await NotifyTransitionListeners(previous.Configuration.ExitListeners, transition);
                }

                //fire entry events on new state
                await NotifyTransitionListeners(currentState.Configuration.EntryListeners, transition);

                //fire generic state machine level events
                RaiseTransitionEvents(transition);

                if (postAction != null) postAction(transition);

                return transition;
            }
            catch (Exception e)
            {
                //publish the exception
                var errorData = new TransitionErrorData<TState>
                {
                    Exception = e,
                    StateMachine = this,
                    StateSwitchPerformed = switchPerformed,
                    FromState = previous == null ? default(TState) : previous.Value,
                    ToState = state,
                    TransitionParameter = parameter is Nothing
                        ? TransitionParameter.NotSet()
                        : TransitionParameter.Set(parameter)
                };

                Configuration.FireTransitionErrorEvent(errorData);

                //rethrow in order to fail the calling task
                throw;
            }
        }

        protected virtual void RaiseTransitionEvents(IStateTransition<TState> transition)
        {
            var handler = StateTransition;
            if (handler != null)
            {
                handler(transition);
            }
        }

        /// <summary>
        /// Resolves all listeners registered for a given transitional event.
        /// </summary>
        /// <param name="listeners">The potential listeners to be invoked.</param>
        /// <param name="transition">The currently processed transition.</param>
        private async Task NotifyTransitionListeners(IEnumerable<TransitionListener<TState>> listeners, IStateTransition<TState> transition)
        {
            var matchingListeners = listeners
                .Where(l => l.Predicate == null || l.Predicate(transition))
                .ToArray();

            foreach (var listener in matchingListeners)
            {
                await listener.Execute(transition);
            }
        }

        private bool EvaluatePreviewListeners(IEnumerable<TransitionListener<TState>> listeners, IStateTransition<TState> transition)
        {
            foreach (var transitionListener in listeners)
            {
                bool status = transitionListener.Predicate(transition);
                if (!status) return false;
            }

            return true;
        }
    }
}