﻿namespace TinyStateMachine
{
    /// <summary>
    /// Reflects a transition from one state to another.
    /// </summary>
    public interface IStateTransition<TState>
    {
        /// <summary>
        /// The state machine that handled the transition.
        /// </summary>
        IStateMachine<TState> StateMachine { get; }

        /// <summary>
        /// Provides convenient access to the <see cref="IStateMachine{TState}.Parameters"/>
        /// dictionary.
        /// </summary>
        ParameterDictionary StateMachineData { get; }

        /// <summary>
        /// The state that from which the transition originated.
        /// </summary>
        IState<TState> PreviousState { get; }

        /// <summary>
        /// The new state the state machine transitioned to.
        /// </summary>
        IState<TState> NewState { get; }
    }
}