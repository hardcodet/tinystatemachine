﻿using System;
using System.Threading.Tasks;

namespace TinyStateMachine
{
    /// <summary>
    /// A running instance of a state machine.
    /// </summary>
    /// <typeparam name="TState">The type of the machine's states. Could be strings, enums,
    /// or any other object / interface.</typeparam>
    public interface IStateMachine<TState>
    {
        /// <summary>
        /// The current state.
        /// </summary>
        IState<TState> CurrentState { get; }

        //TODO canceled/error state handlers, need to go to different state?

        /// <summary>
        /// TODO allow additional event previews for validation hooks, transformations etc
        /// An event that fires upon any state transition of the state machine.
        /// </summary>
        event Action<IStateTransition<TState>>  StateTransition;

        /// <summary>
        /// A generic state bag that provides strongly-typed key-value pairs.
        /// </summary>
        ParameterDictionary Parameters { get; }

        /// <summary>
        /// Enqueues the transition into a different state.
        /// </summary>
        /// <param name="state">The state to transition to.</param>
        /// <returns>A task that indicates the completion of the state transition.</returns>
        Task<IStateTransition<TState>> GoToStateAsync(TState state);

        /// <summary>
        /// Enqueues the transition into a different state with a
        /// complementary parameter value.
        /// </summary>
        /// <param name="state">The state to transition to.</param>
        /// <param name="parameter">An optional payload that is being stored with
        /// the transition.</param>
        /// <returns>A task that indicates the completion of the state transition.</returns>
        Task<IStateTransition<TState>> GoToStateAsync(TState state, object parameter);

        /// <summary>
        /// Triggers a state transition using a trigger value that corresponds to a trigger
        /// condition specified through the state machine configuration.
        /// </summary>
        /// <typeparam name="TTrigger">Trigger value type.</typeparam>
        /// <param name="triggerValue">The value that should cause a state transition. The trigger value will be
        /// attached to the <see cref="IState{TState}.Parameter"/> of the new state.</param>
        /// <returns>A task that indicates the success of the transition along with the actual transition
        /// data.</returns>
        Task TriggerAsync<TTrigger>(TTrigger triggerValue);
    }
}
