﻿using System;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Represents a single instance of a given state.
    /// </summary>
    public interface IState<out TState>
    {
        /// <summary>
        /// Gets the actual state value.
        /// </summary>
        TState Value { get; }

        /// <summary>
        /// Gets the parameter or trigger value that was submitted or caused the
        /// state transition. If no parameter is set, the <see cref="Parameter{T}.HasValue"/>
        /// property will be <c>false</c>.
        /// </summary>
        TransitionParameter Parameter { get; }

        //TODO allow equality check for values

        //TODO
        //void Cancel();
        //void Complete();
        //void Error(Exception exception);
    }

    internal class State<TState> : IState<TState>
    {
        internal StateConfiguration<TState> Configuration { get; private set; }

        /// <summary>
        /// Gets the actual state value.
        /// </summary>
        public TState Value { get; private set; }

        /// <summary>
        /// Gets the parameter or trigger value that was submitted or caused the
        /// state transition. If no parameter is set, the <see cref="TransitionParameter.HasValue"/>
        /// property will be <c>false</c>.
        /// </summary>
        public TransitionParameter Parameter { get; private set; }

        public State(TState value, StateConfiguration<TState> configuration)
        {
            Value = value;
            Configuration = configuration;
            Parameter = TransitionParameter.NotSet();
        }

        internal void SetParameter(object parameterValue)
        {
            Parameter = TransitionParameter.Set(parameterValue);
        }


        public override bool Equals(object obj)
        {
            var other = obj as IState<TState>;
            if (other == null) return false;

            return Configuration.EqualityComparer.Equals(Value, other.Value);
        }

        public override int GetHashCode()
        {
            return Value == null ? base.GetHashCode() : Value.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("{{State:{0}-{1}}}", Value, Parameter);
        }
    }
}