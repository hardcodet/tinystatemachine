﻿namespace TinyStateMachine
{
    /// <summary>
    /// Encapsulates state information to be distributed on a given trigger.
    /// </summary>
    public interface ITriggerData<TState, out TTrigger>
    {
        /// <summary>
        /// The state machine that handled the transition.
        /// </summary>
        IStateMachine<TState> StateMachine { get; }

        /// <summary>
        /// Provides convenient access to the <see cref="IStateMachine{TState}.Parameters"/>
        /// dictionary.
        /// </summary>
        ParameterDictionary StateMachineData { get; }

        /// <summary>
        /// The state that from which the trigger originated.
        /// </summary>
        IState<TState> CurrentState { get; }

        /// <summary>
        /// The received trigger value.
        /// </summary>
        TTrigger TriggerValue { get; }
    }


    internal class TriggerData<TState, TTrigger> : ITriggerData<TState, TTrigger>
    {
        /// <summary>
        /// The state machine that handled the transition.
        /// </summary>
        public IStateMachine<TState> StateMachine { get; }

        /// <summary>
        /// Provides convenient access to the <see cref="IStateMachine{TState}.Parameters"/>
        /// dictionary.
        /// </summary>
        public ParameterDictionary StateMachineData
        {
            get { return StateMachine.Parameters; }
        }

        /// <summary>
        /// The state that from which the trigger originated.
        /// </summary>
        public IState<TState> CurrentState { get; }

        /// <summary>
        /// The received trigger value.
        /// </summary>
        public TTrigger TriggerValue { get; }

        public TriggerData(IStateMachine<TState> stateMachine, IState<TState> currentState, TTrigger triggerValue)
        {
            StateMachine = stateMachine;
            CurrentState = currentState;
            TriggerValue = triggerValue;
        }
    }
}