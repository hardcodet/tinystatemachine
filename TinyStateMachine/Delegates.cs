﻿
namespace TinyStateMachine
{
    /// <summary>
    /// Listener for transition exceptions, which allows ad-hoc correction of states.
    /// </summary>
    /// <typeparam name="TState">Reflects the managed state values.</typeparam>
    /// <param name="errorData">Encapsulates information about the exception and the requested
    /// transition.</param>
    public delegate void TransitionErrorHandler<TState>(TransitionErrorData<TState> errorData);
}
