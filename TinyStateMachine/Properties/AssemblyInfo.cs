﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Tiny State Machine for .NET")]
[assembly: AssemblyDescription("A minimalistic yet flexible state machine for .NET.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("http://www.hardcodet.net")]
[assembly: AssemblyProduct("TinyStateMachine")]
[assembly: AssemblyCopyright("Copyright © 2015 Philipp Sumi")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("37fe624c-b9a5-44e9-bc49-4d4fd6a1029e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly:InternalsVisibleTo("TinyStateMachine.UnitTests")]
