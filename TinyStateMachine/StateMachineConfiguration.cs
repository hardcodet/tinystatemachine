using System;
using System.Collections.Generic;
using System.Linq;

namespace TinyStateMachine
{
    /// <summary>
    /// The "blue print" of a a given state machine instance, which defines known
    /// states and transition behavior.
    /// </summary>
    public class StateMachineConfiguration<TState>
    {
        internal ICollection<StateConfiguration<TState>> Configurations { get; private set; }

        /// <summary>
        /// Whether transitions into unconfigured states (state values)
        /// are being allowed or not. Defaults to <c>true</c>, which disallows
        /// unknown states.
        /// </summary>
        public bool Strict { get; set; }

        /// <summary>
        /// The comparer that is used when looking up states. Uses a default comparer
        /// if not configured.
        /// </summary>
        public IEqualityComparer<TState> EqualityComparer { get; private set; }

        /// <summary>
        /// Fires if an unexpected exception occurrs when transitioning states.
        /// </summary>
        public event TransitionErrorHandler<TState> TransitionError;

        /// <summary>
        /// Creates the configuration.
        /// </summary>
        /// <param name="strict">Whether transitions into unconfigured states (state values)
        /// are being allowed or not. Defaults to <c>true</c>.</param>
        /// <param name="comparer">The comparer that is used when looking up states. Can be specified in order
        /// to properly identify custom state classes, or case-insensitive string comparison (<see cref="StringComparer"/>).
        /// Uses a default comparer if not configured.</param>
        public StateMachineConfiguration(bool strict = true, IEqualityComparer<TState> comparer = null)
        {
            Strict = strict;
            Configurations = new List<StateConfiguration<TState>>();
            EqualityComparer = comparer ?? EqualityComparer<TState>.Default;
        }

        /// <summary>
        /// Creates a configuration for a given state (state value).
        /// </summary>
        /// <param name="state">An actual state value. <c>null</c> is an option if
        /// <typeparamref name="TState"/> is nullable!</param>
        protected StateConfiguration<TState> GetOrCreateStateConfiguration(TState state)
        {
            //try to get an existing configuration
            var configuration = Configurations.FirstOrDefault(c => EqualityComparer.Equals(c.RepresentedState, state));
            if (configuration == null)
            {
                configuration = CreateConfiguration(state);
                Configurations.Add(configuration);
            }
            
            return configuration;
        }

        private StateConfiguration<TState> CreateConfiguration(TState state)
        {
            //lookup the state value
            string name = GetValueName(state);
            return new StateConfiguration<TState>(state, name, EqualityComparer);
        }


        internal void FireTransitionErrorEvent(TransitionErrorData<TState> errorData)
        {
            var handler = TransitionError;
            if (handler != null)
            {
                handler(errorData);
            }
        }
        
        /// <summary>
        /// Attempts to resolve a the configuration for a given <paramref name="state"/>.
        /// </summary>
        /// <param name="state">The state value to be resolved. May be null.</param>
        /// <returns>The resolved configuration, if any.</returns>
        /// <exception cref="InvalidOperationException">If the configuration is strict, and no
        /// matching state was found - or - if multiple configurations were resolved for the state
        /// value.</exception>
        internal StateConfiguration<TState> ResolveConfiguration(TState state)
        {
            //resolve configurations
            var matchingConfigurations = Configurations.Where(c => EqualityComparer.Equals(state, c.RepresentedState)).ToArray();

            if (matchingConfigurations.Count() > 1)
            {
                //TODO unit test
                throw new InvalidOperationException("More than one maching configuration"); //TODO better message with config names
            }

            var configuration = matchingConfigurations.FirstOrDefault();

            if (configuration == null)
            {
                //if there is no configuration, and the behavior is strict, throw exception //TODO unit test
                if (Strict)
                {
                    string msg = "Unconfigured/unknown state '{0}'. Either register this state value," +
                                 " or change the state machine not to be strict.";
                    msg = String.Format(msg, GetValueName(state));
                    throw new InvalidOperationException(msg);
                }

                //create ad-hoc configuration //TODO make sure to test for ad-hoc
                configuration = CreateConfiguration(state);
            }

            return configuration;
        }

        private static string GetValueName(TState value)
        {
            return value == null ? "[null]" : value.ToString();
        }


        public StateSetup<TState> ConfigureState(TState state)
        {
            var config = GetOrCreateStateConfiguration(state);
            return new StateSetup<TState>(config);
        }
    }
}