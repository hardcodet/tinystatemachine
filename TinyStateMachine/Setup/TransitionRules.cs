﻿using System.Linq;
using TinyStateMachine.Util;

namespace TinyStateMachine.Rules
{
    public static class TransitionRules
    {
        /// <summary>
        /// Configures that a transition to a given <paramref name="targetState"/> is valid.
        /// </summary>
        /// <remarks> Enabling transitions is only required on strict state machines
        /// (see <see cref="StateMachineConfiguration{TState}.Strict"/>).</remarks>
        public static StateSetup<TState> AllowTransitionToState<TState>(this StateSetup<TState> configuration, TState targetState)
        {
            return configuration.AllowTransitionTo(t => configuration.Configuration.EqualityComparer.Equals(t.NewState.Value, targetState));
        }

        /// <summary>
        /// Configures the state machine to allow reentry of the state on itself.
        /// </summary>
        /// <remarks> Enabling transitions is only required on strict state machines
        /// (see <see cref="StateMachineConfiguration{TState}.Strict"/>).</remarks>
        public static StateSetup<TState> AllowTransitionToSelf<TState>(this StateSetup<TState> configuration)
        {
            return configuration.AllowTransitionTo(t => configuration.Configuration.EqualityComparer.Equals(t.NewState.Value, configuration.Configuration.RepresentedState));
        }

        /// <summary>
        /// Configures that a transition to a given number of <paramref name="targetStates"/> is valid.
        /// </summary>
        /// <remarks> Enabling transitions is only required on strict state machines
        /// (see <see cref="StateMachineConfiguration{TState}.Strict"/>).</remarks>
        public static StateSetup<TState> AllowTransitionToStates<TState>(this StateSetup<TState> configuration, params TState[] targetStates)
        {
            Guard.NotNull(targetStates, () => targetStates);
            return configuration.AllowTransitionTo(t => targetStates.Contains(t.NewState.Value, configuration.Configuration.EqualityComparer));
        }
    }
}
