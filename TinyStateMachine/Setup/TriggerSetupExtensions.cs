using System;

namespace TinyStateMachine
{
    public static class TriggerSetupExtensions
    {
        public const string COMPLETION_TRIGGER = "_COMPLETED_";

        public static TriggerFuncSetup<TState, TTrigger, bool> ThenIfTrue<TState, TTrigger>(this TriggerFuncSetup<TState, TTrigger, bool> setup, Action<ITriggerData<TState, TTrigger>> action)
        {
            return setup.Then((td, result) =>
            {
                if (!result) return;
                action(td);
            });
        }

        public static TriggerFuncSetup<TState, TTrigger, bool> ThenIfFalse<TState, TTrigger>(this TriggerFuncSetup<TState, TTrigger, bool> setup, Action<ITriggerData<TState, TTrigger>> action)
        {
            return setup.Then((td, result) =>
            {
                if (result) return;
                action(td);
            });
        }

        public static TriggerFuncSetup<TState, TTrigger, bool> ThenConditionalGoToState<TState, TTrigger>(this TriggerFuncSetup<TState, TTrigger, bool> setup, TState trueState, TState falseState)
        {
            return setup.Then((td, result) =>
            {
                TState state = result ? trueState : falseState;
                td.StateMachine.GoToStateAsync(state);
            });
        }


        public static TriggerSetup<TState, string> OnCompleted<TState>(this StateSetup<TState> setup)
        {
            return setup.OnTrigger(COMPLETION_TRIGGER);
        }

    }
}