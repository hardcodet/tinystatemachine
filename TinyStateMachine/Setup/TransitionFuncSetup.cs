using System;
using System.Threading.Tasks;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Configures a function that is being invoked during a state transition.
    /// </summary>
    public class TransitionFuncSetup<TState, TResult> : TransitionSetup<TState>
    {
        internal TransitionCommand<TState, TResult> TransitionCommand { get; } = new TransitionCommand<TState, TResult>();


        internal TransitionFuncSetup(TransitionCommand<TState, TResult> transitionCommand, TransitionListener<TState> transitionListener, StateConfiguration<TState> configuration) : base(transitionListener, configuration)
        {
            TransitionCommand = transitionCommand;
        }

        /// <summary>
        /// Executes a given action before continuing processing.
        /// </summary>
        public TransitionFuncSetup<TState, TResult> Then(Action<IStateTransition<TState>, TResult> action)
        {
            Guard.NotNull(action, () => action);
            return ThenAsync((td, r) => Task.Run(() => action(td, r)));
        }

        /// <summary>
        /// Runs and awaits a task before continuing processing.
        /// </summary>
        public TransitionFuncSetup<TState, TResult> ThenAsync(Func<IStateTransition<TState>, TResult, Task> taskFunc)
        {
            Guard.NotNull(taskFunc, () => taskFunc);

            //create a transition command and submit the Transition as a parameter
            TransitionCommand.FollowUpCommands.Add(taskFunc);
            return this;
        }

        /// <summary>
        /// Enqueues a request for a state transition, which will be processed once pending transactions have
        /// been completed.
        /// </summary>
        public TransitionFuncSetup<TState, TResult> ThenEnqueueGoToState(TState state)
        {
            return Then((td, r) => td.StateMachine.GoToStateAsync(state));
        }

        /// <summary>
        /// Enqueues a request for a state transition, which will be processed once pending transactions have
        /// been completed.
        /// </summary>
        public TransitionFuncSetup<TState, TResult> ThenEnqueueGoToState(TState state, object parameter)
        {
            return Then((td, r) => td.StateMachine.GoToStateAsync(state, parameter));
        }
    }
}