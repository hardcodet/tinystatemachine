﻿using System;
using System.Threading.Tasks;
using TinyStateMachine.Rules;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    public class TriggerSetup<TState, TTrigger> : StateSetup<TState>
    {
        internal TriggerListener<TState> TriggerListener { get; private set; }

        internal TriggerSetup(TriggerListener<TState> triggerListener, StateConfiguration<TState> configuration) : base(configuration)
        {
            TriggerListener = triggerListener;
        }

        /// <summary>
        /// Enqueues a request to transitions the state machine to the specified state if the
        /// trigger is fired. Note that the request is not awaited in order to avoid
        /// deadlocks with nested requests / triggers.
        /// </summary>
        public TriggerSetup<TState, TTrigger> EnqueueGoToState(TState nextState)
        {
            //implicitly unlock transition even if it wasn't configured just yet
            this.AllowTransitionToState(nextState);

            //create a transition command and submit the trigger as a parameter
            TriggerCommand<TState, TTrigger> cmd = new TriggerCommand<TState, TTrigger>();
            cmd.TriggerAction = td =>
            {
                //enqueue as an independent task in order to avoid deadlocks and return
                //and non-blocking task instead
                td.StateMachine.GoToStateAsync(nextState, td.TriggerValue);
                return Task.Delay(0);
            };
            TriggerListener.TriggerCommands.Add(cmd);

            return this;
        }


        /// <summary>
        /// Executes an arbitrary action when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TriggerSetup<TState, TTrigger> Execute(Action<ITriggerData<TState, TTrigger>> action)
        {
            Guard.NotNull(action, () => action);
            return ExecuteAsync(td => Task.Run(() => action(td)));
        }

        /// <summary>
        /// Runs and awaits a task when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TriggerSetup<TState, TTrigger> ExecuteAsync(Func<ITriggerData<TState, TTrigger>, Task> taskFunc)
        {
            Guard.NotNull(taskFunc, () => taskFunc);

            //create a transition command and submit the trigger as a parameter
            TriggerCommand<TState, TTrigger> cmd = new TriggerCommand<TState, TTrigger>();
            cmd.TriggerAction = taskFunc;
            TriggerListener.TriggerCommands.Add(cmd);

            return this;
        }

        /// <summary>
        /// Executes an arbitrary func when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TriggerFuncSetup<TState, TTrigger, TResult> ExecuteFunc<TResult>(Func<ITriggerData<TState, TTrigger>, TResult> func)
        {
            Guard.NotNull(func, () => func);
            return ExecuteFuncAsync(td => Task.Run(() => func(td)));
        }

        /// <summary>
        /// Executes an arbitrary func when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TriggerFuncSetup<TState, TTrigger, TResult> ExecuteFuncAsync<TResult>(Func<ITriggerData<TState, TTrigger>, Task<TResult>> func)
        {
            Guard.NotNull(func, () => func);

            //create a transition command and submit the trigger as a parameter
            TriggerCommand<TState, TTrigger, TResult> cmd = new TriggerCommand<TState, TTrigger, TResult>();
            cmd.TriggerFunc = func;
            TriggerListener.TriggerCommands.Add(cmd);

            return new TriggerFuncSetup<TState, TTrigger, TResult>(cmd, TriggerListener, Configuration);
        }
    }
}
