using System;

namespace TinyStateMachine
{
    public static class TransitionSetupExtensions
    {
        public static TransitionFuncSetup<TState, bool> ThenIfTrue<TState>(this TransitionFuncSetup<TState, bool> setup, Action<IStateTransition<TState>> action)
        {
            return setup.Then((td, result) =>
            {
                if (!result) return;
                action(td);
            });
        }

        public static TransitionFuncSetup<TState, bool> ThenIfFalse<TState>(this TransitionFuncSetup<TState, bool> setup, Action<IStateTransition<TState>> action)
        {
            return setup.Then((td, result) =>
            {
                if (result) return;
                action(td);
            });
        }

        public static TransitionFuncSetup<TState, bool> ThenConditionalGoToState<TState>(this TransitionFuncSetup<TState, bool> setup, TState trueState, TState falseState)
        {
            return setup.Then((td, result) =>
            {
                TState state = result ? trueState : falseState;
                td.StateMachine.GoToStateAsync(state);
            });
        }

    }
}