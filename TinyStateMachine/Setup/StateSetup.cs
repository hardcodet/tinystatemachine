using System;
using System.Collections.Generic;
using System.Linq;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Provides an API to configure a given state of the state machine.
    /// </summary>
    public partial class StateSetup<TState>
    {
        internal StateConfiguration<TState> Configuration { get; set; }

        internal StateSetup(StateConfiguration<TState> configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Sets a descriptive for the state in order to simplify debugging and logging.
        /// If not set, a string representation of the state value will be used.
        /// </summary>
        /// <param name="name">A descriptive name for the state.</param>
        public StateSetup<TState> WithName(string name)
        {
            Configuration.Name = Guard.NotNull(name, () => name);
            return this;
        }

        /// <summary>
        /// Registers a callback listener that is being invoked before the state machine transitions into this
        /// state.
        /// </summary>
        /// <param name="previewHandler">A callback / validation handler, which can interrupt the transition
        /// by returning <c>false</c>.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="previewHandler"/> is a null reference.</exception>
        public StateSetup<TState> PreviewOnEntry(Func<IStateTransition<TState>, bool> previewHandler)
        {
            Guard.NotNull(previewHandler, () => previewHandler);

            var listener = new TransitionListener<TState>(previewHandler);
            Configuration.EntryPreviewListeners.Add(listener);

            return this;
        }

        /// <summary>
        /// Registers a callback listener that is being invoked before the state machine transitions into a new
        /// state.
        /// </summary>
        /// <param name="previewHandler">A callback / validation handler, which can interrupt the transition
        /// by returning <c>false</c>.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="previewHandler"/> is a null reference.</exception>
        public StateSetup<TState> PreviewOnExit(Func<IStateTransition<TState>, bool> previewHandler)
        {
            Guard.NotNull(previewHandler, () => previewHandler);

            var listener = new TransitionListener<TState>(previewHandler);
            Configuration.ExitPreviewListeners.Add(listener);

            return this;
        }

        /// <summary>
        /// Registers a callback listener that is being invoked on state exit.
        /// </summary>
        /// <param name="filter">An optional predicate which evaluates whether the callback listener should be
        /// invoked at all or not.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="exitAction"/> is a null reference.</exception>
        public StateSetup<TState> OnExit(Action<IStateTransition<TState>> exitAction, Func<IStateTransition<TState>, bool> filter = null)
        {
            TransitionSetup<TState> setup = OnExit(filter);
            setup.Execute(exitAction);
            return this;
        }

        /// <summary>
        /// Registers a callback listener that is being invoked on state exit.
        /// </summary>
        /// <param name="exitAction">The action to be invoked when the state machine transitions out of this state.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <param name="filter">An optional predicate which evaluates whether the callback listener should be
        /// invoked at all or not.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="exitAction"/> is a null reference.</exception>
        public TransitionSetup<TState> OnExit(Func<IStateTransition<TState>, bool> filter = null)
        {
            return CreateListener(filter, Configuration.ExitListeners);
        }

        /// <summary>
        /// Registers a callback listener that is being invoked on state entry.
        /// </summary>
        /// <param name="entryAction">The action to be invoked when the state machine enters this state.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <param name="filter">An optional predicate which evaluates whether the callback listener should be
        /// invoked at all or not.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="entryAction"/> is a null reference.</exception>
        public StateSetup<TState> OnEntry(Action<IStateTransition<TState>> entryAction, Func<IStateTransition<TState>, bool> filter = null)
        {
            TransitionSetup<TState> setup = OnEntry(filter);
            setup.Execute(entryAction);
            return this;
        }


        /// <summary>
        /// Registers a callback listener that is being invoked on state entry.
        /// </summary>
        /// <param name="filter">An optional predicate which evaluates whether the callback listener should be
        /// invoked at all or not.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="entryAction"/> is a null reference.</exception>
        public TransitionSetup<TState> OnEntry(Func<IStateTransition<TState>, bool> filter = null)
        {
            return CreateListener(filter, Configuration.EntryListeners);
        }

        /// <summary>
        /// Registers a callback listener that is being invoked when entering this state, coming from a specific
        /// other state.
        /// </summary>
        /// <param name="previousState">The originating state of the state machine.</param>
        /// <param name="entryAction">The action to be invoked when the state machine enters this state.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="entryAction"/> is a null reference.</exception>
        public TransitionSetup<TState> OnEntryFrom(TState previousState)
        {
            Func<IStateTransition<TState>, bool> filter = t => Configuration.EqualityComparer.Equals(t.PreviousState.Value, previousState);
            return OnEntry(filter);
        }

        /// <summary>
        /// Registers a callback listener that is being invoked when entering this state, coming from a specific
        /// other state.
        /// </summary>
        /// <param name="previousState">The originating state of the state machine.</param>
        /// <param name="entryAction">The action to be invoked when the state machine enters this state.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="entryAction"/> is a null reference.</exception>
        public StateSetup<TState> OnEntryFrom(TState previousState, Action<IStateTransition<TState>> entryAction)
        {
            Func<IStateTransition<TState>, bool> filter = t => Configuration.EqualityComparer.Equals(t.PreviousState.Value, previousState);
            return OnEntry(entryAction, filter);
        }
        
        /// <summary>
        /// Registers a callback listener that is being invoked when transitioning into a specific other state.
        /// </summary>
        /// <param name="nextState">The new state of the state machine.</param>
        /// <param name="exitAction">The action to be invoked when the state machine enters this state.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="exitAction"/> is a null reference.</exception>
        public TransitionSetup<TState> OnExitTo(TState nextState)
        {
            Func<IStateTransition<TState>, bool> filter = t => Configuration.EqualityComparer.Equals(t.NewState.Value, nextState);
            return OnExit(filter);
        }

        /// <summary>
        /// Registers a callback listener that is being invoked when transitioning into a specific other state.
        /// </summary>
        /// <param name="nextState">The new state of the state machine.</param>
        /// <param name="exitAction">The action to be invoked when the state machine enters this state.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="exitAction"/> is a null reference.</exception>
        public StateSetup<TState> OnExitTo(TState nextState, Action<IStateTransition<TState>> exitAction)
        {
            Func<IStateTransition<TState>, bool> filter = t => Configuration.EqualityComparer.Equals(t.NewState.Value, nextState);
            return OnExit(exitAction, filter);
        }


        private TransitionSetup<TState> CreateListener(Func<IStateTransition<TState>, bool> filter, ICollection<TransitionListener<TState>> targetCollection)
        {
            var listener = new TransitionListener<TState>(filter);
            targetCollection.Add(listener);

            return new TransitionSetup<TState>(listener, Configuration);
        }


        public TriggerSetup<TState, TTrigger> OnTrigger<TTrigger>(TTrigger triggerValue)
        {
            return ConfigureTrigger<TTrigger>(o => Equals(o, triggerValue));
        }

        public TriggerSetup<TState, TTrigger> OnTriggers<TTrigger>(params TTrigger[] values)
        {
            return ConfigureTrigger<TTrigger>(o => o is TTrigger && values.Any(v => Equals(o, v)));
        }

        public TriggerSetup<TState, TTrigger> OnTrigger<TTrigger>()
        {
            return ConfigureTrigger<TTrigger>(o => o is TTrigger);
        }

        public TriggerSetup<TState, TTrigger> OnTrigger<TTrigger>(Func<TTrigger, bool> predicate)
        {
            return ConfigureTrigger<TTrigger>(o => o is TTrigger && predicate((TTrigger)o));
        }

        private TriggerSetup<TState, TTrigger> ConfigureTrigger<TTrigger>(Func<object, bool> predicate)
        {
            var triggerConfiguration = new TriggerListener<TState>(predicate);
            Configuration.TriggerConfigurations.Add(triggerConfiguration);

            return new TriggerSetup<TState, TTrigger>(triggerConfiguration, Configuration);
        }

        /// <summary>
        /// Defines whether a transition to a given other state can be performed or not.
        /// This is a required setting for strict state machines - every transition that is not
        /// explicitly allowed, will be forbidden.
        /// </summary>
        /// <param name="predicate">Validates whether the target state is allowed or not.</param>
        /// <returns>The configuration object, in order to simplify chaining.</returns>
        /// <remarks> Enabling transitions is only required on strict state machines
        /// (see <see cref="StateMachineConfiguration{TState}.Strict"/>).</remarks>
        public StateSetup<TState> AllowTransitionTo(Func<IStateTransition<TState>, bool> predicate)
        {
            return CreateListener(predicate, Configuration.TransitionRules);
        }
    }
}