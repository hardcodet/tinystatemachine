using System;
using System.Threading.Tasks;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Configures listeners to be invoked on a given trigger.
    /// </summary>
    public class TriggerFuncSetup<TState, TTrigger, TResult> : TriggerSetup<TState, TTrigger>
    {
        internal TriggerCommand<TState, TTrigger, TResult> TriggerCommand { get; } = new TriggerCommand<TState, TTrigger, TResult>();


        internal TriggerFuncSetup(TriggerCommand<TState, TTrigger, TResult> triggerCommand, TriggerListener<TState> triggerListener, StateConfiguration<TState> configuration) : base(triggerListener, configuration)
        {
            TriggerCommand = triggerCommand;
        }

        /// <summary>
        /// Executes a given action before continuing processing.
        /// </summary>
        public TriggerFuncSetup<TState, TTrigger, TResult> Then(Action<ITriggerData<TState, TTrigger>, TResult> action)
        {
            Guard.NotNull(action, () => action);
            return ThenAsync((td, r) => Task.Run(() => action(td, r)));
        }

        /// <summary>
        /// Runs and awaits a task before continuing processing.
        /// </summary>
        public TriggerFuncSetup<TState, TTrigger, TResult> ThenAsync(Func<ITriggerData<TState, TTrigger>, TResult, Task> taskFunc)
        {
            Guard.NotNull(taskFunc, () => taskFunc);

            //create a transition command and submit the trigger as a parameter
            TriggerCommand.FollowUpCommands.Add(taskFunc);
            return this;
        }

        /// <summary>
        /// Enqueues a request for a state transition, which will be processed once pending transactions have
        /// been completed.
        /// </summary>
        public TriggerFuncSetup<TState, TTrigger, TResult> ThenEnqueueGoToState(TState state)
        {
            return Then((td, r) => td.StateMachine.GoToStateAsync(state));
        }

        /// <summary>
        /// Enqueues a request for a state transition, which will be processed once pending transactions have
        /// been completed.
        /// </summary>
        public TriggerFuncSetup<TState, TTrigger, TResult> ThenEnqueueGoToState(TState state, object parameter)
        {
            return Then((td, r) => td.StateMachine.GoToStateAsync(state, parameter));
        }
    }
}