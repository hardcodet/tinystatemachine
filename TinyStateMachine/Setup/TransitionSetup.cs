using System;
using System.Threading.Tasks;
using TinyStateMachine.Rules;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Configures listeners to be invoked during a state transition.
    /// </summary>
    public class TransitionSetup<TState> : StateSetup<TState>
    {
        internal TransitionListener<TState> TransitionListener { get; private set; }

        internal TransitionSetup(TransitionListener<TState> transitionListener, StateConfiguration<TState> configuration) : base(configuration)
        {
            TransitionListener = transitionListener;
        }

        /// <summary>
        /// Enqueues a request to transitions the state machine to the specified state if the
        /// trigger is fired. Note that the request is not awaited in order to avoid
        /// deadlocks with nested requests / triggers.
        /// </summary>
        public TransitionSetup<TState> EnqueueGoToState(TState nextState)
        {
            //implicitly unlock transition even if it wasn't configured just yet
            this.AllowTransitionToState(nextState);

            //create a transition command and submit the trigger as a parameter
            TransitionCommand<TState> cmd = new TransitionCommand<TState>();
            cmd.TransitionAction = td =>
            {
                //enqueue as an independent task in order to avoid deadlocks and return
                //and non-blocking task instead
                td.StateMachine.GoToStateAsync(nextState);
                return Task.Delay(0);
            };

            TransitionListener.TransitionCommands.Add(cmd);
            return this;
        }

        /// <summary>
        /// Executes an arbitrary action when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TransitionSetup<TState> Execute(Action<IStateTransition<TState>> action)
        {
            Guard.NotNull(action, () => action);
            return ExecuteAsync(td => Task.Run(() => action(td)));
        }

        /// <summary>
        /// Runs and awaits a task when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TransitionSetup<TState> ExecuteAsync(Func<IStateTransition<TState>, Task> taskFunc)
        {
            Guard.NotNull(taskFunc, () => taskFunc);

            //create a transition command and submit the trigger as a parameter
            TransitionCommand<TState> cmd = new TransitionCommand<TState>();
            cmd.TransitionAction = taskFunc;
            TransitionListener.TransitionCommands.Add(cmd);

            return this;
        }

        /// <summary>
        /// Executes an arbitrary func when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TransitionFuncSetup<TState, TResult> ExecuteFunc<TResult>(Func<IStateTransition<TState>, TResult> func)
        {
            Guard.NotNull(func, () => func);
            return ExecuteFuncAsync(td => Task.Run(() => func(td)));
        }

        public TransitionFuncSetup<TState, TParameter> ReadParameter<TParameter>(bool throwException = true)
        {
            Func<IStateTransition<TState>, bool> filter = t =>
            {
                TransitionParameter parameter = t.NewState.Parameter;
                bool status = (parameter.HasValue && (parameter.Value is TParameter || parameter.Value == null && default(TParameter) == null));

                if (!status && throwException)
                {
                    string msg = "Transition to state {0} is lacking expected parameter/trigger of type {1}.";
                    msg = String.Format(msg, t.NewState, typeof (TParameter).Name);
                    throw new InvalidOperationException(msg);
                }

                return status;
            };

            TransitionCommand<TState, TParameter> command = new TransitionCommand<TState, TParameter>();
            command.Predicate = filter;
            command.CommandFunc = st => Task.FromResult((TParameter)st.NewState.Parameter.Value);
            TransitionListener.TransitionCommands.Add(command);

            return new TransitionFuncSetup<TState, TParameter>(command, TransitionListener, Configuration);
        }

        /// <summary>
        /// Executes an arbitrary func when the trigger fires. Actions
        /// are being executed in declaration order.
        /// </summary>
        public TransitionFuncSetup<TState, TResult> ExecuteFuncAsync<TResult>(Func<IStateTransition<TState>, Task<TResult>> func)
        {
            Guard.NotNull(func, () => func);

            //create a transition command and submit the trigger as a parameter
            TransitionCommand<TState, TResult> cmd = new TransitionCommand<TState, TResult>();
            cmd.CommandFunc = func;
            TransitionListener.TransitionCommands.Add(cmd);

            return new TransitionFuncSetup<TState, TResult>(cmd, TransitionListener, Configuration);
        }
    }
}