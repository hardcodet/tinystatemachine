using System;

namespace TinyStateMachine
{
    /// <summary>
    /// A parameter that can be used to maintain state as a key/value pair.
    /// </summary>
    public interface IParameter
    {
        /// <summary>
        /// The parameter name, which should be unique
        /// within its current context.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The parameter value.
        /// </summary>
        object Value { get; }

        /// <summary>
        /// Gets the type of the parameter's value.
        /// </summary>
        /// <remarks>
        /// This parameter may be explicitly set in case the
        /// <see cref="IParameter.Value"/> is null</remarks>
        Type ValueType { get; }
    }
}