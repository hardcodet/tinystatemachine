﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Encapsulates collection of named <see cref="IParameter"/> instances.
    /// </summary>
    public class ParameterDictionary : IEnumerable<IParameter>
    {
        /// <summary>
        /// Stores maintained parameters by their <see cref="IParameter.Name"/>.
        /// </summary>
        private readonly Dictionary<string, IParameter> parameterCache = new Dictionary<string, IParameter>();

        /// <summary>
        /// Inits an empty collection.
        /// </summary>
        public ParameterDictionary()
        {
        }

        /// <summary>
        /// Creates the collection and immediately imports the submitted
        /// <paramref name="parameters"/>.
        /// </summary>
        /// <param name="parameters"></param>
        /// <exception cref="ArgumentNullException">If <paramref name="parameters"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameters"/>
        /// contains duplicate parameters.</exception>
        public ParameterDictionary(IEnumerable<IParameter> parameters)
        {
            AddRange(parameters);
        }

        /// <summary>
        /// Appends a number of parameters to the collection.
        /// </summary>
        /// <param name="parameters">Parameters to be appended to the collection.</param>
        /// <param name="overwrite">Whether to silently overwrite existing parameters.
        /// Set to <c>false</c> to cause an exception instead.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="parameters"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameters"/>
        /// contains duplicates
        /// -or-
        /// if <paramref name="overwrite"/> is true and <paramref name="parameters"/>
        /// contains parameters that duplicate existing entries of this collection.</exception>
        public void AddRange(IEnumerable<IParameter> parameters, bool overwrite = true)
        {
            Guard.NotNull(parameters, () => parameters);


            parameters = parameters.ToArray();
            
            //make sure the collection does not contain null entries
            if (parameters.Any(p => p == null))
            {
                string msg = "Submitted parameter collection contains null references.";
                throw new ArgumentException(msg);
            }

            //make sure the collection does not contain any duplicates itself
            IEnumerable<string> duplicates = (from p in parameters
                let key = GetKey(p)
                group p by key
                into g
                where g.Count() > 1
                select g.Key).ToArray();


            if (duplicates.Any())
            {
                string msg = "Submitted parameters contain duplicated keys [{0}].";
                msg = String.Format(msg, String.Join(",", duplicates));
                throw new ArgumentException(msg);
            }


            if (overwrite)
            {
                //if overwriting is allowed, just replace
                foreach (var parameter in parameters)
                {
                    Add(parameter, true);
                }
                return;
            }

            duplicates = from p in parameters
                         let key = GetKey(p)
                         where Contains(key)
                         select p.Name;

            //make sure none of the parameter names is already in the dictionary
            if (duplicates.Any())
            {
                string msg = "Submitted parameters duplicate existing entries of the collection [{0}].";
                msg = String.Format(msg, String.Join(",", duplicates));
                throw new ArgumentException(msg);
            }

            foreach (IParameter parameter in parameters)
            {
                //some overhead, but shouldn't be a problem...
                Add(parameter);
            }
        }

        /// <summary>
        /// Creates a parameter on the fly and assigns it to the collection.
        /// </summary>
        /// <typeparam name="T">The type of the parameter value.</typeparam>
        /// <param name="parameterName">The <see cref="IParameter.Name"/> of the
        /// created parameter.</param>
        /// <param name="parameterValue">The <see cref="IParameter.Value"/> of the
        /// created parameter.</param>
        /// <param name="overwrite">Whether to silently overwrite an existing parameter of the
        /// same <see cref="IParameter.Name"/>. Set to false to cause an exception
        /// in case of duplicate parameters.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="parameterName"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameterName"/> is
        /// an empty string.</exception>
        /// <exception cref="ArgumentException">If the another parameter with the
        /// same <see cref="IParameter.Name"/> has already been assigned to the collection.</exception>
        public void Add<T>(string parameterName, T parameterValue, bool overwrite = true)
        {
            Add(Parameter.Create(parameterName, parameterValue), overwrite);
        }

        /// <summary>
        /// Assigns a new parameter to the collection.
        /// </summary>
        /// <param name="parameter">The parameter instance to be added to the
        /// collection.</param>
        /// <param name="overwrite">Whether to silently overwrite an existing parameter of the
        /// same <see cref="IParameter.Name"/>. Set to false to cause an exception
        /// in case of duplicate parameters.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="parameter"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If the another parameter with the
        /// same <see cref="IParameter.Name"/> has already been assigned to the collection.</exception>
        public void Add(IParameter parameter, bool overwrite = true)
        {
            Guard.NotNull(parameter, () => parameter);

            string key = GetKey(parameter);
            if (Contains(key) && !overwrite)
            {
                string msg = String.Format("Duplicate parameter key [{0}].", key);
                throw new ArgumentException(msg);
            }

            parameterCache[key] = parameter;
        }

        /// <summary>
        /// Removes a parameter with a given <paramref name="parameterName"/>
        /// from the collection.
        /// </summary>
        /// <param name="parameterName">The <see cref="IParameter.Name"/> of the
        /// parameter to be removed.</param>
        /// <returns>True if a matching <see cref="IParameter"/> instance was
        /// found and removed.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="parameterName"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameterName"/>
        /// is an empty string.</exception>
        public bool Remove(string parameterName)
        {
            Guard.NotNullOrEmpty(parameterName, () => parameterName);

            string key = GetKey(parameterName);
            return parameterCache.Remove(key);
        }

        /// <summary>
        /// Verifies whether a given parameter has been registered with the
        /// dictionary.
        /// </summary>
        /// <param name="parameterName">The <see cref="IParameter.Name"/> of the
        /// parameter to be queried.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="parameterName"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameterName"/>
        /// is an empty string.</exception>
        public bool Contains(string parameterName)
        {
            Guard.NotNullOrEmpty(parameterName, () => parameterName);
            return parameterCache.ContainsKey(GetKey(parameterName));
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        public void Clear()
        {
            parameterCache.Clear();
        }

        /// <summary>
        /// Gets the parameter associated with the specified <paramref name="parameterName"/>.
        /// </summary>
        /// <param name="parameterName">The <see cref="IParameter.Name"/> of a registered
        /// parameter (casing does not matter).
        /// </param>
        /// <param name="parameter">When this method returns, contains the value associated
        /// with the specified key, if the key is found; otherwise, the default value for
        /// the type of the value parameter. This parameter is passed uninitialized.
        /// </param>
        /// <returns>True if a matching <see cref="IParameter"/> was found.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="parameterName"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameterName"/> is
        /// an empty string.</exception>
        public bool TryGetParameter(string parameterName, out IParameter parameter)
        {
            string key = GetKey(parameterName);
            return parameterCache.TryGetValue(key, out parameter);
        }

        /// <summary>
        /// Resolves the value of a registered parameter.
        /// </summary>
        /// <param name="parameterName">The <see cref="IParameter.Name"/> of a registered
        /// parameter (casing does not matter).
        /// </param>
        /// <returns>A registered parameter's value, or null, if no matching
        /// parameter can be found.
        /// </returns>
        /// <exception cref="ArgumentNullException">If <paramref name="parameterName"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameterName"/> is
        /// an empty string.</exception>
        public object GetValue(string parameterName)
        {
            return this.GetValue<object>(parameterName);
        }

        /// <summary>
        /// Resolves the value of a registered parameter (including a cast to
        /// <typeparamref name="T"/>), and falls back to the default value of
        /// <typeparamref name="T"/>, if no matching parameter was found.
        /// </summary>
        /// <typeparam name="T">The type of the retrieved parameter value.</typeparam>
        /// <param name="parameterName">The <see cref="IParameter.Name"/> of the requested
        /// parameter.
        /// </param>
        /// <param name="defaultValue">The default value to be used, in case the parameter
        /// is not available.</param>
        /// <returns>The <see cref="IParameter.Value"/>, if a matching parameter was
        /// found. Otherwise the submitted <paramref name="defaultValue"/> is being returned.
        /// </returns>
        /// <exception cref="ArgumentNullException">If <paramref name="parameterName"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="parameterName"/> is
        /// an empty string.</exception>
        /// <exception cref="InvalidCastException">If the <see cref="IParameter.Value"/>
        /// of a found parameter is not a <typeparamref name="T"/>.</exception>
        public T GetValue<T>(string parameterName, T defaultValue = default(T))
        {
            Guard.NotNullOrEmpty(parameterName, () => parameterName);

            IParameter parameter;
            bool status = TryGetParameter(parameterName, out parameter);

            if (!status)
            {
                return defaultValue;
            }

            return (T)parameter.Value;
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        public IEnumerator<IParameter> GetEnumerator()
        {
            return parameterCache.Values.GetEnumerator();
        }

        /// <summary>
        /// Gets the number of <see cref="IParameter"/> instances
        /// contained in the collection.
        /// </summary>
        public int Count
        {
            get { return parameterCache.Count; }
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Resolves a case invariant key that is used to store
        /// parameters in the <see cref="parameterCache"/>.
        /// </summary>
        private static string GetKey(IParameter parameter)
        {
            return parameter.Name.ToUpperInvariant();
        }

        /// <summary>
        /// Resolves a case invariant key that is used to store
        /// parameters in the <see cref="parameterCache"/>.
        /// </summary>
        private static string GetKey(string parameterName)
        {
            return parameterName.ToUpperInvariant();
        }

        /// <summary>
        /// Returns a string representation of the collection that provides a hint about the
        /// contained parameters.
        /// contents.
        /// </summary>
        public override string ToString()
        {
            if (parameterCache.Count == 0)
            {
                //the collection is empty
                return GetType().FullName + " [EMPTY]";
            }

            // return at max 5 params
            var paramInfo = this.Take(5).Select(x => String.Format("{0}={1}", x.Name, x.Value));
            string paramString = String.Join(" | ", paramInfo);
            
            if (parameterCache.Count > 5)
            {
                paramString += " | ...";
            }

            return String.Format("[{0}: {1}]", GetType().Name, paramString);
        }
    }
}
