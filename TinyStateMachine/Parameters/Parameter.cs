﻿using System;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// A parameter that can be used to maintain state as a key/value pair.
    /// </summary>
    public class Parameter : IParameter
    {
        /// <summary>
        /// The parameter name, which should be unique
        /// within its current context (e.g. a request).
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The parameter value.
        /// </summary>
        public object Value { get; private set; }

        /// <summary>
        /// Gets the type of the parameter's value.
        /// </summary>
        /// <remarks>
        /// This parameter may be explicitly set in case the
        /// <see cref="IParameter.Value"/> is null</remarks>
        public Type ValueType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        /// <exception cref="ArgumentNullException">If <paramref name="name"/> or
        /// <paramref name="valueType"/> is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="name"/> is
        /// an empty string.</exception>
        /// <exception cref="ArgumentException">If <paramref name="value"/> and
        /// <paramref name="valueType"/> are incompatible.</exception>
        internal Parameter(string name, object value, Type valueType)
        {
            Guard.NotNullOrEmpty(name, () => name);
            Guard.NotNull(valueType, () => valueType);

            //validate type compatibility (don't create an int parameter with a string value)
            if (value != null)
            {
                Guard.TypeIsA(value.GetType(), valueType, () => valueType);
            }

            Name = name;
            Value = value;
            ValueType = valueType;
        }

        /// <summary>
        /// Creates a parameter with a given name.
        /// </summary>
        /// <typeparam name="T">The type of the parameter value. May be an interface.
        /// </typeparam>
        /// <param name="name">The parameter's <see cref="IParameter.Name"/>.</param>
        /// <param name="value">The actual parameter value. May be null.</param>
        /// <returns>An <see cref="IParameter"/> that encapsulates the submitted value.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="name"/>
        /// is a null reference.</exception>
        /// <exception cref="ArgumentException">If <paramref name="name"/> is
        /// an empty string.</exception>
        public static IParameter Create<T>(string name, T value)
        {
            return new Parameter(name, value, typeof(T));
        }
    }
}