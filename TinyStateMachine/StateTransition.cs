﻿using System.Security.Principal;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Reflects a transition from one state to another.
    /// </summary>
    internal class StateTransition<TState> : IStateTransition<TState>
    {
        /// <summary>
        /// The state machine that handled the transition.
        /// </summary>
        public IStateMachine<TState> StateMachine { get; private set; }

        /// <summary>
        /// The state from which the transition originated.
        /// </summary>
        public IState<TState> PreviousState { get; private set; }

        /// <summary>
        /// The new state the state machine transitioned to.
        /// </summary>
        public IState<TState> NewState { get; private set; }

        /// <summary>
        /// Provides convenient access to the <see cref="IStateMachine{TState}.Parameters"/>
        /// dictionary.
        /// </summary>
        public ParameterDictionary StateMachineData
        {
            get { return StateMachine.Parameters; }
        }

        /// <summary>
        /// Creates a new state transition.
        /// </summary>
        /// <param name="stateMachine">The state machine that handled the transition.</param>
        /// <param name="previousState">The state from which the transition originated.</param>
        /// <param name="newState">The new state the state machine transitioned to.</param>
        public StateTransition(IStateMachine<TState> stateMachine, IState<TState> previousState, IState<TState> newState)
        {
            StateMachine = stateMachine;
            PreviousState = previousState;
            NewState = Guard.NotNull(newState, () => newState);
        }
    }

    internal class TransitionPreview<TState> : StateTransition<TState>
    {
        public bool IsDenied { get; private set; }
        public bool DenialMessage { get; private set; }

        public TransitionPreview(IStateMachine<TState> stateMachine, IState<TState> previousState, IState<TState> newState) : base(stateMachine, previousState, newState)
        {
        }

        void DenyTransition(string message = null)
        {
            
        }
    }
}