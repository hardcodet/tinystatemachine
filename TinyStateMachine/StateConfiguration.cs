using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TinyStateMachine.Util;

namespace TinyStateMachine
{
    /// <summary>
    /// Internal configuration object that contains the setup of a given state.
    /// </summary>
    /// <typeparam name="TState">Type of the maintained states.</typeparam>
    public class StateConfiguration<TState>
    {
        /// <summary>
        /// The comparer that is used when looking up states.
        /// </summary>
        public IEqualityComparer<TState> EqualityComparer { get; private set; }

        /// <summary>
        /// The represented state value.
        /// </summary>
        public TState RepresentedState { get; private set; }

        /// <summary>
        /// A descriptive name for the state - used for logging and debugging.
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// A collection of listeners that may be invoked when transitioning into the
        /// state.
        /// </summary>     
        internal ICollection<TransitionListener<TState>> EntryListeners { get; private set; }

        /// <summary>
        /// A collection of preview listeners that may be invoked when transitioning into the
        /// state.
        /// </summary>     
        internal ICollection<TransitionListener<TState>> EntryPreviewListeners { get; private set; }

        /// <summary>
        /// A collection of listeners that may be invoked when transitioning out of the
        /// state.
        /// </summary>   
        internal ICollection<TransitionListener<TState>> ExitListeners { get; private set; }

        /// <summary>
        /// A collection of listeners that may be invoked when transitioning out of the
        /// state.
        /// </summary>   
        internal ICollection<TransitionListener<TState>> ExitPreviewListeners { get; private set; }

        /// <summary>
        /// A collection of rules that declare whether the transition from the configured
        /// state to another state is valid. At least one rule must match the targeted state,
        /// or the requested transition is regareded invalid.
        /// </summary>   
        internal ICollection<TransitionListener<TState>> TransitionRules { get; private set; }

        /// <summary>
        /// Trigger configurations which describe possible triggers that result into transitions
        /// into this state.
        /// </summary>
        internal ICollection<TriggerListener<TState>> TriggerConfigurations { get; private set; }

        /// <summary>
        /// Creates a configuration for a number of state values, which are
        /// evaluated at runtime, but should be treated all the same.
        /// </summary>
        /// <param name="name">A descriptive name of the state. Mandatory in order to simplify
        /// debugging and logging.</param>
        /// <param name="representedState">The configured state.</param>
        /// <param name="comparer">The comparer that is used when looking up states.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="name"/> or
        /// <paramref name="comparer"/> is a null reference.</exception>
        internal StateConfiguration(TState representedState, string name, IEqualityComparer<TState> comparer)
        {
            Name = Guard.NotNull(name, () => name);
            EqualityComparer = Guard.NotNull(comparer, () => comparer);
            RepresentedState = representedState;

            EntryListeners = new List<TransitionListener<TState>>();
            ExitListeners = new List<TransitionListener<TState>>();
            EntryPreviewListeners = new List<TransitionListener<TState>>();
            ExitPreviewListeners = new List<TransitionListener<TState>>();
            TransitionRules = new List<TransitionListener<TState>>();
            TriggerConfigurations = new List<TriggerListener<TState>>();
        }


        /// <summary>
        /// Resolves a configuration name with a fallback to the <see cref="RepresentedState"/>
        /// value if the <see cref="Name"/> property isn't set.
        /// </summary>
        public string ResolveName()
        {
            if (!String.IsNullOrEmpty(Name)) return Name;

            var state = RepresentedState == null ? "NULL" : RepresentedState.ToString();
            if (state.Length <= 15) return state;

            return String.Format("{0}...", state.Substring(0, 15 - 3));
        }
    }
}