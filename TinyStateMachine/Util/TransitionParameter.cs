﻿using System;

namespace TinyStateMachine.Util
{
    /// <summary>
    /// Represents an optional value that may or may not be set.
    /// Check the <see cref="HasValue"/> property before attempting
    /// to access the <see cref="Value"/> property accordingly.
    /// </summary>
    public class TransitionParameter
    {
        /// <summary>
        /// Indicates whether a <see cref="Value"/> was assigned or not.
        /// </summary>
        public bool HasValue { get; private set; }

        private object _value;

        /// <summary>
        /// Gets the assigned value (which may be null, if <c>T</c> is a reference type).
        /// </summary>
        /// <exception cref="InvalidOperationException">If <see cref="HasValue"/> is false
        /// and no value was assigned in the first place.</exception>
        public object Value
        {
            get
            {
                if(!HasValue) throw new InvalidOperationException("No value was assigned.");
                return _value;
            }
            private set { _value = value; }
        }

        /// <summary>
        /// Creates an empty optional that does not have a value assigned.
        /// </summary>
        private TransitionParameter(bool hasValue, object value = null)
        {
            HasValue = hasValue;
            Value = value;
        }

        /// <summary>
        /// Creates a new <see cref="TransitionParameter"/> with a specific value.
        /// </summary>
        /// <param name="value">The assigned value. Null is a valid value, if <typeparamref name="T"/>
        /// is a reference type.</param>
        public static TransitionParameter Set(object value)
        {
            return new TransitionParameter(true, value);
        }

        /// <summary>
        /// Creates an empty <see cref="TransitionParameter"/> that does not have a value assigned.
        /// </summary>
        public static TransitionParameter NotSet()
        {
            return new TransitionParameter(false);
        }

        /// <summary>
        /// Tries to retrieve a parameter value of a given type, and reverts
        /// to the submitted <paramref name="defaultValue"/> if the parameter
        /// is not set.
        /// If the parameter is set, but not assignable to the specified type
        /// parameter, a casting exception will occur.
        /// </summary>
        /// <param name="defaultValue">The default value to be returned if the
        /// parameter is not available. If not specified, the default (null for
        /// reference types) of the requested type will be returned.</param>
        /// <returns>The casted parameter value, or a default, if the parameter
        /// is not set.</returns>
        /// <exception cref="InvalidCastException">If the parameter value is not
        /// of type <typeparamref name="T"/>.</exception>
        public T TryGetValue<T>(T defaultValue = default(T))
        {
            return HasValue ? (T) _value : defaultValue;
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            TransitionParameter other = obj as TransitionParameter;
            if (other == null) return false;

            if (!HasValue) return other.HasValue == false;

            //compare values
            return Equals(Value, other.Value);
        }

        public override int GetHashCode()
        {
            return Value == null ? base.GetHashCode() : Value.GetHashCode();
        }

        public override string ToString()
        {
            if (!HasValue) return "";
            return Value.ToString();
        }
    }
}