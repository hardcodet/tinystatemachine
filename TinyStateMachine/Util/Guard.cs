﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;

namespace TinyStateMachine.Util
{
    /// <summary>
    /// Performs common parameter/argument validation tasks.
    /// </summary>
    [DebuggerStepThrough]
    internal static class Guard
    {
        /// <summary>
        /// Verifies the <paramref name="argumentValue"/> is not <c>null</c> and throws an <see cref="ArgumentNullException"/> if it is <c>null</c>.
        /// </summary>
        /// <typeparam name="T">The type of the <paramref name="argumentValue"/> to verify.</typeparam>
        /// <param name="argumentValue">The value to verify.</param>
        /// <param name="argumentName">The name of the <paramref name="argumentValue"/>.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="argumentValue"/> parameter is <c>null</c>.</exception>
        public static T NotNull<T>(T argumentValue, string argumentName) where T : class
        {
            if (argumentValue == null)
            {
                throw new ArgumentNullException(argumentName);
            }

            return argumentValue;
        }

        /// <summary>
        /// Makes sure a given argument is not null.
        /// </summary>
        /// <typeparam name="T">Type of the argument.</typeparam>
        /// <param name="argumentValue">The evaluated argument value.</param>
        /// <param name="argumentExpression">A <see cref="MemberExpression"/> that refers
        /// to the validated argument.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="argumentExpression"/>
        /// links to a null reference.</exception>
        public static T NotNull<T>(T argumentValue, Expression<Func<T>> argumentExpression) where T : class
        {
            if (argumentValue == null)
            {
                string argumentName = GetParameterName(argumentExpression);
                throw new ArgumentNullException(argumentName);
            }

            return argumentValue;
        }

        /// <summary>
        /// Verifies the <paramref name="argumentValue"/> is not <c>null</c> or an empty string and throws an <see cref="ArgumentNullException"/> if
        /// it is <c>null</c> or an <see cref="ArgumentException"/> if it is an empty string.
        /// </summary>
        /// <param name="argumentValue">The value to verify.</param>
        /// <param name="argumentName">The name of the <paramref name="argumentValue"/>.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="argumentValue"/> parameter is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">The <paramref name="argumentValue"/> parameter is an empty string.</exception>
        public static string NotNullOrEmpty(string argumentValue, string argumentName)
        {
            NotNull(argumentValue, argumentName);
            if (argumentValue.Length == 0)
            {
                const string msg = "String argument must not be empty.";
                throw new ArgumentException(msg, argumentName);
            }

            return argumentValue;
        }

        /// <summary>
        /// Makes sure a given argument is not <c>null</c> or an empty string and throws an <see cref="ArgumentNullException"/> if
        /// it is <c>null</c> or an <see cref="ArgumentException"/> if it is an empty string.
        /// </summary>
        /// <param name="argumentValue"> The argument value that is evaluated.</param>
        /// <param name="argumentExpression"> A <see cref="MemberExpression"/> that refers
        /// to the parameter to be validated.</param>
        /// <exception cref="ArgumentNullException"> If <paramref name="argumentExpression"/>
        /// links to a null reference.</exception>
        /// <exception cref="ArgumentException"> If <paramref name="argumentExpression"/>resolves to
        /// an empty string.</exception>
        public static string NotNullOrEmpty(string argumentValue, Expression<Func<string>> argumentExpression)
        {
            NotNull(argumentValue, argumentExpression);
            if (argumentValue.Length == 0)
            {
                string argumentName = GetParameterName(argumentExpression);
                const string msg = "String argument must not be empty.";
                throw new ArgumentException(msg, argumentName);
            }

            return argumentValue;
        }

        /// <summary>
        /// Verifies whether a given argument's type is assignable to the <paramref name="contractType"/>
        /// (meaning interfaces are implemented,  or classes exist in the base class hierarchy).
        /// </summary>
        /// <param name="argumentType">A given type to be validated.</param>
        /// <param name="contractType">The base or target type that the <paramref name="argumentType"/>
        /// will be assigned to.</param>
        /// <param name="argumentExpression">A <see cref="MemberExpression"/> that refers
        /// to the validated argument.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="argumentType"/>
        /// or <paramref name="contractType"/> is a null reference.</exception>
        public static void TypeIsA(Type argumentType, Type contractType, Expression<Func<Type>> argumentExpression)
        {
            NotNull(argumentType, "argumentType");
            NotNull(contractType, "contractType");

            if (!contractType.IsAssignableFrom(argumentType))
            {
                string parameterName = GetParameterName(argumentExpression);
                string msg = "Type argument [{0}] with value [{1}] is neither a [{2}] nor a sub type.";
                msg = String.Format(msg, parameterName, argumentType, contractType);
                throw new ArgumentException(msg);
            }
        }

        /// <summary>
        /// Verifies whether a given argument's type is assignable to the <paramref name="contractType"/>
        /// (meaning interfaces are implemented,  or classes exist in the base class hierarchy).
        /// </summary>
        /// <param name="argumentType">A given type to be validated.</param>
        /// <param name="contractType">The target type that will be assigned to.</param>
        /// <param name="argumentName">The name of the verified argument.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="argumentType"/>,
        /// or <paramref name="contractType"/> is a null reference.</exception>
        public static void TypeIsA(Type argumentType, Type contractType, string argumentName)
        {
            NotNull(argumentType, "argumentType");
            NotNull(contractType, "contractType");
            
            if (!contractType.IsAssignableFrom(argumentType))
            {
                string msg = "Type argument [{0}] with value [{1}] is neither a [{2}] nor a sub type.";
                msg = String.Format(msg, argumentName, argumentType, contractType);
                throw new ArgumentException(msg);
            }
        }

        /// <summary>
        /// Resolves the argument name from a given <paramref name="argumentExpression"/>.
        /// If the expression happens to be null, a null reference is returned, which
        /// is equivalent to not supplying an argument name.
        /// </summary>
        private static string GetParameterName<T>(Expression<Func<T>> argumentExpression)
        {
            if (argumentExpression == null) return null;

            // process the expression in order to get parameter name and value
            var exp = (MemberExpression)argumentExpression.Body;
            return exp.Member.Name;
        }
    }
}