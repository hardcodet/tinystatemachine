﻿namespace TinyStateMachine.Util
{
    /// <summary>
    /// Represents a value that is basically nothing. Can be used
    /// in places where <c>null</c> is a valid value, but a distinction
    /// needs to be made between assigning null and not having assigned
    /// anything.
    /// </summary>
    internal class Nothing
    {
        /// <summary>
        /// Singleton instance of the <see cref="Nothing"/> value.
        /// </summary>
        public static readonly Nothing Instance = new Nothing();

        private Nothing()
        {
        }
    }
}