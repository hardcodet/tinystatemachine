using System;
using System.Threading.Tasks;

namespace TinyStateMachine.Util
{
    /// <summary>
    /// A simple queue that allows to enqueue tasks in order to
    /// ensure that unrelated tasks are being executed in sequential
    /// order.
    /// </summary>
    public class TaskQueue
    {
        /// <summary>
        /// Used in order to look queue processing.
        /// </summary>
        private readonly object syncRoot = new object();

        /// <summary>
        /// Used in order to look queue processing.
        /// </summary>
        protected internal object SyncRoot
        {
            get { return syncRoot; }
        }

        /// <summary>
        /// The most recent running / pending task, if any.
        /// </summary>
        public Task PendingTask { get; private set; }

        /// <summary>
        /// Enqueues a pending action, which will be executed after the currently
        /// pending task(s).
        /// </summary>
        /// <param name="action">An action to be executed after the currently
        /// last pending task.</param>
        /// <returns>A task that wraps the submitted <paramref name="action"/>.</returns>
        public virtual Task EnqueueTask(Action action)
        {
            return EnqueueTask(() => Task.Run(action));
        }

        /// <summary>
        /// Enqueues a pending action, which will be executed after the currently
        /// pending task(s).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func">An function to be executed after the currently
        /// last pending task.</param>
        /// <returns>A task that wraps the submitted <paramref name="func"/>, and
        /// returns it's value.</returns>
        public virtual Task<T> EnqueueTask<T>(Func<T> func)
        {
            return EnqueueTask(() => Task.Run(func));
        }

        /// <summary>
        /// Enqueues a pending task, thus ensuring sequential execution of tasks.
        /// </summary>
        public virtual Task<T> EnqueueTask<T>(Func<Task<T>> taskFunc)
        {
            lock (SyncRoot)
            {
                //if we are currently working on a task, just add the returned task as a continuation of that
                //task and make it the new pending task
                Task<T> task = PendingTask == null || PendingTask.IsCompleted ? taskFunc() : PendingTask.ContinueWith(t => taskFunc()).Unwrap();

                PendingTask = task;

                //register continuation that cleans up after us
                task.ContinueWith(t =>
                    {
                        lock (SyncRoot)
                        {
                            if (ReferenceEquals(t, PendingTask)) PendingTask = null;
                        }
                    });
                return task;
            }
        }

        /// <summary>
        /// Enqueues a pending task, thus ensuring sequential execution of tasks.
        /// </summary>
        /// <param name="taskFunc">A function that eventually creates the task to
        /// be executed. Using a func rather than a concrete instance ensures that
        /// execution can be scheduled accordingly.</param>
        public virtual Task EnqueueTask(Func<Task> taskFunc)
        {
            lock (SyncRoot)
            {
                //if we are currently working on a task, just add the returned task as a continuation of that
                //task and make it the new pending task
                Task task = PendingTask == null || PendingTask.IsCompleted ? taskFunc() : PendingTask.ContinueWith(t => taskFunc()).Unwrap();

                PendingTask = task;

                //register continuation that cleans up after us
                task.ContinueWith(t =>
                    {
                        lock (SyncRoot)
                        {
                            if (ReferenceEquals(t, PendingTask)) PendingTask = null;
                        }
                    });

                return task;
            }
        }
    }
}